function [  ] = demo()

file = 'samples/kcsa/final_KcsA.pdb';
file1 = 'samples/2RVB/2RVB.pdb';
file2 = 'samples/2LWW/2LWW.pdb';
file3 = 'samples/2MP2/2MP2.pdb';
file4 = 'samples/2myj/2myj.pdb';
file5 = 'samples/2k1e/2k1e.pdb';
file6 = 'samples/2m3x/2m3x.pdb';
file7 = 'samples/2mxu/2mxu.pdb';
file8 = 'samples/test/2m3b.pdb';
toolboxPath = fileparts(which('calmodulin_demo'));

[initialPDBstruct, firstConf, lastConf] = getData(file6); 

nConf = 8;
model = trmcreate(firstConf, lastConf, nConf);

% angles = [500, 500];
% nAngles = getNumAng(model,angles);
nAngles = [0,0,0,1,0,1,1,0,0,1,1,0];
transformations = trmvariateinterpolationsinbonds(model, nAngles);

%nAngles = 50*ones(1, 4);
for i=1:length(transformations)
    
%     angleIndices = trmdistantangleindices(transformations{i});
%     f = @(x) trmobjfunc(transformations{i}, angleIndices, x);
% 
%     initial_point = createInitialPoint(transformations{i}, angleIndices);
%     
%     iterNum = 400;
%     options = optimoptions('fmincon');
%     options = optimoptions(options,'Display', 'iter');
%     options = optimoptions(options,'MaxIter', iterNum);
%     options = optimoptions(options,'MaxFunEvals', Inf);
%     options = optimoptions(options,'GradObj','off');
%     options = optimoptions(options,'UseParallel',true);
%     
%     [lowerBound, upperBound] = getBounds(transformations{i}, angleIndices);
%     
%     if max(size(gcp)) == 0 
%         parpool 
%     end
%     
%     x = fmincon(f,initial_point,[],[],[],[],lowerBound, upperBound,[],...
%         options);
%     
%     modelOptimized = modelOpt(transformations{i}, angleIndices, x);
%   
    newPDB = trm2pdb(transformations{i}, initialPDBstruct);
    name = strcat('samples/2m3x/varinterp2/2m3x_',int2str(i), '_inter_3rd.pdb'); 
    pdbwrite(fullfile(toolboxPath, name), newPDB);
    
%     if pdbmininteratomicdist(newPDB) <= 1
%         disp([name, ' not looking so good']);
%     end
    
    %delete(gcp);
end

end