HEADER    HORMONE                                 05-SEP-07   2JUV              
TITLE     ABAA3-DKP-INSULIN                                                     
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: INSULIN A CHAIN;                                           
COMPND   3 CHAIN: A;                                                            
COMPND   4 ENGINEERED: YES;                                                     
COMPND   5 MUTATION: YES;                                                       
COMPND   6 MOL_ID: 2;                                                           
COMPND   7 MOLECULE: INSULIN B CHAIN;                                           
COMPND   8 CHAIN: B;                                                            
COMPND   9 ENGINEERED: YES;                                                     
COMPND  10 MUTATION: YES                                                        
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 SYNTHETIC: YES;                                                      
SOURCE   3 MOL_ID: 2;                                                           
SOURCE   4 SYNTHETIC: YES                                                       
KEYWDS    INSULIN, NMR, ABA, CARBOHYDRATE METABOLISM, CLEAVAGE ON               
KEYWDS   2 PAIR OF BASIC RESIDUES, DIABETES MELLITUS, DISEASE MUTATION          
KEYWDS   3 GLUCOSE METABOLISM, HORMONE, PHARMACEUTICAL, SECRETED                
EXPDTA    SOLUTION NMR                                                          
AUTHOR    K.HUANG,S.CHAN,Q.HUA,Y.CHU,R.WANG,B.KLAPROTH,W.JIA,                   
AUTHOR   2 J.WHITTAKER,P.DE MEYTS,S.H.NAKAGAWA,D.F.STEINER,                     
AUTHOR   3 P.G.KATSOYANNIS,M.A.WEISS                                            
REVDAT   3   24-FEB-09 2JUV    1       VERSN                                    
REVDAT   2   11-DEC-07 2JUV    1       JRNL                                     
REVDAT   1   16-OCT-07 2JUV    0                                                
JRNL        AUTH   K.HUANG,S.J.CHAN,Q.X.HUA,Y.C.CHU,R.Y.WANG,                   
JRNL        AUTH 2 B.KLAPROTH,W.JIA,J.WHITTAKER,P.DE MEYTS,                     
JRNL        AUTH 3 S.H.NAKAGAWA,D.F.STEINER,P.G.KATSOYANNIS,M.A.WEISS           
JRNL        TITL   THE A-CHAIN OF INSULIN CONTACTS THE INSERT DOMAIN            
JRNL        TITL 2 OF THE INSULIN RECEPTOR: PHOTO-CROSS-LINKING AND             
JRNL        TITL 3 MUTAGENESIS OF A DIABETES-RELATED CREVICE.                   
JRNL        REF    J.BIOL.CHEM.                  V. 282 35337 2007              
JRNL        REFN                   ISSN 0021-9258                               
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : CNS, X-PLOR 3.1                                      
REMARK   3   AUTHORS     : ACCELRYS (CNS), BRUNGER, A.T. ET AL. (X-PLO          
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: NULL                                      
REMARK   4                                                                      
REMARK   4 2JUV COMPLIES WITH FORMAT V. 3.15, 01-DEC-08                         
REMARK 100                                                                      
REMARK 100 THIS ENTRY HAS BEEN PROCESSED BY RCSB ON 20-SEP-07.                  
REMARK 100 THE RCSB ID CODE IS RCSB100319.                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 308; 315; 308                      
REMARK 210  PH                             : 7.0; 8.0; 1.9                      
REMARK 210  IONIC STRENGTH                 : NULL; NULL; NULL                   
REMARK 210  PRESSURE                       : AMBIENT; AMBIENT; AMBIENT          
REMARK 210  SAMPLE CONTENTS                : 1 MM ABAA3-DKP-INSULIN, 9          
REMARK 210                                   H2O/10% D2O; 1 MM ABAA3-D          
REMARK 210                                   INSULIN, 100% D2O; 1 MM A          
REMARK 210                                   DKP-INSULIN, 100% D2O; 1           
REMARK 210                                   ABAA3-DKP-INSULIN, 20% D-          
REMARK 210                                   ACETIC ACID/80% H2O; 1 MM          
REMARK 210                                   ABAA3-DKP-INSULIN, 20%D-A          
REMARK 210                                   ACID/80% D2O                       
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : 2D 1H-1H COSY, 2D 1H-1H            
REMARK 210                                   TOCSY, 2D 1H-1H NOESY, 2D          
REMARK 210                                   COSY                               
REMARK 210  SPECTROMETER FIELD STRENGTH    : 600 MHZ, 800 MHZ                   
REMARK 210  SPECTROMETER MODEL             : INOVA, DMX                         
REMARK 210  SPECTROMETER MANUFACTURER      : VARIAN, BRUKER                     
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : NULL                               
REMARK 210   METHOD USED                   : DGSA-DISTANCE GEOMETRY,            
REMARK 210                                   SIMULATED ANNEALING                
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 50                                 
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 15                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : STRUCTURES WITH THE LOWES          
REMARK 210                                   ENERGY                             
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : 1                   
REMARK 210                                                                      
REMARK 210 REMARK: NULL                                                         
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 THE COORDINATES IN THIS ENTRY WERE GENERATED FROM SOLUTION           
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 215 THESE RECORDS ARE MEANINGLESS.                                       
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500 CHOLOGY: RAMACHANDRAN REVISITED. STRUCTURE 4, 1395 - 1400            
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  1 SER A   9     -114.32   -123.75                                   
REMARK 500  1 VAL B   2     -141.12   -138.03                                   
REMARK 500  1 ASN B   3       61.78   -112.61                                   
REMARK 500  1 CYS B   7      140.88    157.64                                   
REMARK 500  1 CYS B  19      -71.06   -100.28                                   
REMARK 500  1 TYR B  26      109.04     54.47                                   
REMARK 500  1 THR B  27     -169.41   -109.25                                   
REMARK 500  1 LYS B  28      -57.92   -168.22                                   
REMARK 500  2 SER A   9     -139.13   -117.55                                   
REMARK 500  2 ASN B   3       60.04   -175.29                                   
REMARK 500  2 CYS B   7      138.24    159.38                                   
REMARK 500  2 CYS B  19      -64.53    -92.09                                   
REMARK 500  2 PRO B  29       49.78    -81.10                                   
REMARK 500  3 SER A   9     -130.01   -119.52                                   
REMARK 500  3 VAL B   2     -161.37   -106.07                                   
REMARK 500  3 ASN B   3       63.47   -112.70                                   
REMARK 500  3 CYS B   7      149.57    158.84                                   
REMARK 500  3 CYS B  19      -75.83   -106.95                                   
REMARK 500  3 LYS B  28      -60.09   -154.71                                   
REMARK 500  4 SER A   9     -122.34   -130.61                                   
REMARK 500  4 ASN B   3       55.55   -110.33                                   
REMARK 500  4 CYS B   7      138.36    162.09                                   
REMARK 500  4 CYS B  19      -67.83    -95.41                                   
REMARK 500  5 SER A   9     -139.07   -123.29                                   
REMARK 500  5 CYS B   7      142.31    163.95                                   
REMARK 500  5 CYS B  19      -70.55   -132.71                                   
REMARK 500  6 SER A   9     -101.15   -119.63                                   
REMARK 500  6 ASN B   3       64.64   -101.25                                   
REMARK 500  6 HIS B   5     -166.33    -69.26                                   
REMARK 500  6 CYS B   7      155.68    169.21                                   
REMARK 500  6 CYS B  19      -76.56   -130.40                                   
REMARK 500  6 PRO B  29      -90.55    -80.06                                   
REMARK 500  7 SER A   9      -98.36   -121.57                                   
REMARK 500  7 CYS B   7      135.47    159.89                                   
REMARK 500  7 TYR B  26       96.70     59.54                                   
REMARK 500  8 SER A   9     -105.68   -127.05                                   
REMARK 500  8 VAL B   2      -93.62   -130.65                                   
REMARK 500  8 ASN B   3       75.27     70.46                                   
REMARK 500  8 HIS B   5     -179.14    -67.37                                   
REMARK 500  8 CYS B   7      153.24    166.95                                   
REMARK 500  8 CYS B  19      -70.44   -129.34                                   
REMARK 500  8 PHE B  25       67.83   -115.06                                   
REMARK 500  9 SER A   9     -104.39   -145.79                                   
REMARK 500  9 LEU A  13       -8.10    -56.66                                   
REMARK 500  9 ASN B   3       57.12   -154.26                                   
REMARK 500  9 CYS B   7      135.15    158.42                                   
REMARK 500  9 CYS B  19      -61.26   -147.35                                   
REMARK 500 10 SER A   9     -107.66   -110.24                                   
REMARK 500 10 CYS B   7      134.07    144.48                                   
REMARK 500 10 LYS B  28      -60.28   -158.57                                   
REMARK 500 11 SER A   9     -110.88   -124.34                                   
REMARK 500 11 LEU A  13       -8.60    -59.31                                   
REMARK 500 11 ASN B   3       57.97   -160.51                                   
REMARK 500 11 CYS B   7      143.18    163.48                                   
REMARK 500 11 LYS B  28      -59.70   -156.02                                   
REMARK 500 12 SER A   9      -97.49   -146.39                                   
REMARK 500 12 ASN B   3       54.49   -174.12                                   
REMARK 500 12 CYS B   7      145.46    162.03                                   
REMARK 500 12 CYS B  19      -53.50   -137.45                                   
REMARK 500 13 SER A   9     -125.75   -130.81                                   
REMARK 500 13 CYS A  20      174.89    -52.18                                   
REMARK 500 13 VAL B   2      -84.46   -159.70                                   
REMARK 500 13 ASN B   3       79.67     68.14                                   
REMARK 500 13 CYS B   7      137.49    157.95                                   
REMARK 500 13 PHE B  25       69.81   -115.36                                   
REMARK 500 14 SER A   9     -129.25   -121.42                                   
REMARK 500 14 VAL B   2     -160.66   -160.12                                   
REMARK 500 14 HIS B   5     -161.52    -67.70                                   
REMARK 500 14 CYS B   7      152.70    170.08                                   
REMARK 500 14 CYS B  19      -76.27   -133.67                                   
REMARK 500 15 CYS A   7      -32.30   -144.69                                   
REMARK 500 15 SER A   9     -138.48    -99.66                                   
REMARK 500 15 LEU A  13       -8.03    -58.23                                   
REMARK 500 15 ASN B   3       58.55   -144.33                                   
REMARK 500 15 CYS B  19      -71.19   -137.29                                   
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: PLANAR GROUPS                                              
REMARK 500                                                                      
REMARK 500 PLANAR GROUPS IN THE FOLLOWING RESIDUES HAVE A TOTAL                 
REMARK 500 RMS DISTANCE OF ALL ATOMS FROM THE BEST-FIT PLANE                    
REMARK 500 BY MORE THAN AN EXPECTED VALUE OF 6*RMSD, WITH AN                    
REMARK 500 RMSD 0.02 ANGSTROMS, OR AT LEAST ONE ATOM HAS                        
REMARK 500 AN RMSD GREATER THAN THIS VALUE                                      
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        RMS     TYPE                                    
REMARK 500  1 ARG B  22         0.10    SIDE_CHAIN                              
REMARK 500  2 ARG B  22         0.20    SIDE_CHAIN                              
REMARK 500  3 ARG B  22         0.23    SIDE_CHAIN                              
REMARK 500  4 ARG B  22         0.28    SIDE_CHAIN                              
REMARK 500  5 ARG B  22         0.32    SIDE_CHAIN                              
REMARK 500  7 ARG B  22         0.30    SIDE_CHAIN                              
REMARK 500  8 ARG B  22         0.21    SIDE_CHAIN                              
REMARK 500  9 ARG B  22         0.18    SIDE_CHAIN                              
REMARK 500 10 ARG B  22         0.23    SIDE_CHAIN                              
REMARK 500 11 ARG B  22         0.21    SIDE_CHAIN                              
REMARK 500 12 ARG B  22         0.19    SIDE_CHAIN                              
REMARK 500 13 ARG B  22         0.14    SIDE_CHAIN                              
REMARK 500 14 ARG B  22         0.18    SIDE_CHAIN                              
REMARK 500 15 ARG B  22         0.30    SIDE_CHAIN                              
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 900                                                                      
REMARK 900 RELATED ENTRIES                                                      
REMARK 900 RELATED ID: 2JUM   RELATED DB: PDB                                   
REMARK 900 RELATED ID: 2JUU   RELATED DB: PDB                                   
DBREF  2JUV A    1    21  UNP    P01308   INS_HUMAN       90    110             
DBREF  2JUV B    1    30  UNP    P01308   INS_HUMAN       25     54             
SEQADV 2JUV ABA A    3  UNP  P01308    VAL    92 ENGINEERED                     
SEQADV 2JUV ASP B   10  UNP  P01308    HIS    34 ENGINEERED                     
SEQADV 2JUV LYS B   28  UNP  P01308    PRO    52 ENGINEERED                     
SEQADV 2JUV PRO B   29  UNP  P01308    LYS    53 ENGINEERED                     
SEQRES   1 A   21  GLY ILE ABA GLU GLN CYS CYS THR SER ILE CYS SER LEU          
SEQRES   2 A   21  TYR GLN LEU GLU ASN TYR CYS ASN                              
SEQRES   1 B   30  PHE VAL ASN GLN HIS LEU CYS GLY SER ASP LEU VAL GLU          
SEQRES   2 B   30  ALA LEU TYR LEU VAL CYS GLY GLU ARG GLY PHE PHE TYR          
SEQRES   3 B   30  THR LYS PRO THR                                              
MODRES 2JUV ABA A    3  ALA  ALPHA-AMINOBUTYRIC ACID                            
HET    ABA  A   3      13                                                       
HETNAM     ABA ALPHA-AMINOBUTYRIC ACID                                          
FORMUL   1  ABA    C4 H9 N O2                                                   
HELIX    1   1 GLY A    1  THR A    8  1                                   8    
HELIX    2   2 LEU A   16  CYS A   20  5                                   5    
HELIX    3   3 GLY B    8  CYS B   19  1                                  12    
HELIX    4   4 GLY B   20  GLY B   23  5                                   4    
SSBOND   1 CYS A    6    CYS A   11                          1555   1555        
SSBOND   2 CYS A    7    CYS B    7                          1555   1555        
SSBOND   3 CYS A   20    CYS B   19                          1555   1555        
LINK         C   ILE A   2                 N   ABA A   3     1555   1555        
LINK         C   ABA A   3                 N   GLU A   4     1555   1555        
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
MODEL        1                                                                  
ATOM      1  N   GLY A   1      -0.882  -5.071  10.041  1.00  0.00           N  
ATOM      2  CA  GLY A   1       0.247  -4.068   9.804  1.00  0.00           C  
ATOM      3  C   GLY A   1       0.878  -3.978   8.454  1.00  0.00           C  
ATOM      4  O   GLY A   1       2.056  -4.229   8.291  1.00  0.00           O  
ATOM      5  H1  GLY A   1      -0.875  -5.787   9.286  1.00  0.00           H  
ATOM      6  H2  GLY A   1      -1.795  -4.572  10.037  1.00  0.00           H  
ATOM      7  H3  GLY A   1      -0.746  -5.537  10.961  1.00  0.00           H  
ATOM      8  HA2 GLY A   1      -0.103  -3.186   9.585  1.00  0.00           H  
ATOM      9  HA3 GLY A   1       0.908  -4.068  10.767  1.00  0.00           H  
ATOM     10  N   ILE A   2       0.118  -3.622   7.452  1.00  0.00           N  
HETATM   29  N   ABA A   3       1.740  -1.467   6.795  1.00  0.00           N  
HETATM   30  CA  ABA A   3       2.831  -0.460   6.844  1.00  0.00           C  
HETATM   31  C   ABA A   3       4.111  -1.100   7.371  1.00  0.00           C  
HETATM   32  O   ABA A   3       5.189  -0.863   6.877  1.00  0.00           O  
HETATM   33  CB  ABA A   3       2.411   0.679   7.768  1.00  0.00           C  
HETATM   34  CG  ABA A   3       1.658   1.739   6.964  1.00  0.00           C  
HETATM   35  H   ABA A   3       0.917  -1.313   7.293  1.00  0.00           H  
HETATM   36  HA  ABA A   3       3.007  -0.075   5.858  1.00  0.00           H  
HETATM   37  HB3 ABA A   3       3.284   1.119   8.212  1.00  0.00           H  
HETATM   38  HB2 ABA A   3       1.769   0.291   8.546  1.00  0.00           H  
HETATM   39  HG1 ABA A   3       0.640   1.413   6.804  1.00  0.00           H  
HETATM   40  HG3 ABA A   3       1.656   2.670   7.509  1.00  0.00           H  
HETATM   41  HG2 ABA A   3       2.144   1.880   6.010  1.00  0.00           H  
TER     310      ASN A  21                                                      
ATOM    311  N   PHE B   1       6.082   2.492  -9.775  1.00  0.00           N  
ATOM    312  CA  PHE B   1       6.351   1.592  -8.617  1.00  0.00           C  
ATOM    313  C   PHE B   1       7.641   0.812  -8.872  1.00  0.00           C  
ATOM    314  O   PHE B   1       7.905   0.375  -9.974  1.00  0.00           O  
ATOM    315  CB  PHE B   1       5.184   0.618  -8.451  1.00  0.00           C  
ATOM    316  CG  PHE B   1       3.895   1.323  -8.798  1.00  0.00           C  
ATOM    317  CD1 PHE B   1       3.267   2.140  -7.849  1.00  0.00           C  
ATOM    318  CD2 PHE B   1       3.330   1.163 -10.070  1.00  0.00           C  
ATOM    319  CE1 PHE B   1       2.072   2.796  -8.172  1.00  0.00           C  
ATOM    320  CE2 PHE B   1       2.135   1.818 -10.393  1.00  0.00           C  
TER     780      THR B  30                                                      
ENDMDL                                                                          
MODEL       15                                                                  
ATOM      1  N   GLY A   1      -1.320  -3.743   9.790  1.00  0.00           N  
ATOM      2  CA  GLY A   1       0.197  -3.734   9.600  1.00  0.00           C  
ATOM      3  C   GLY A   1       0.762  -3.613   8.225  1.00  0.00           C  
ATOM      4  O   GLY A   1       1.933  -3.850   8.003  1.00  0.00           O  
ATOM      5  H1  GLY A   1      -1.752  -4.399   9.108  1.00  0.00           H  
ATOM      6  H2  GLY A   1      -1.693  -2.785   9.635  1.00  0.00           H  
ATOM      7  H3  GLY A   1      -1.546  -4.052  10.757  1.00  0.00           H  
ATOM      8  HA2 GLY A   1       0.563  -2.838   9.714  1.00  0.00           H  
ATOM      9  HA3 GLY A   1       0.640  -4.506  10.355  1.00  0.00           H  
ATOM     10  N   ILE A   2      -0.045  -3.245   7.268  1.00  0.00           N  
HETATM   29  N   ABA A   3       1.454  -0.942   6.302  1.00  0.00           N  
HETATM   30  CA  ABA A   3       2.558   0.066   6.309  1.00  0.00           C  
HETATM   31  C   ABA A   3       3.869  -0.607   6.696  1.00  0.00           C  
HETATM   32  O   ABA A   3       4.679  -0.915   5.853  1.00  0.00           O  
HETATM   33  CB  ABA A   3       2.265   1.205   7.303  1.00  0.00           C  
HETATM   34  CG  ABA A   3       0.974   0.940   8.084  1.00  0.00           C  
HETATM   35  H   ABA A   3       0.574  -0.698   6.619  1.00  0.00           H  
HETATM   36  HA  ABA A   3       2.661   0.480   5.322  1.00  0.00           H  
HETATM   37  HB3 ABA A   3       2.168   2.132   6.759  1.00  0.00           H  
HETATM   38  HB2 ABA A   3       3.084   1.285   7.996  1.00  0.00           H  
HETATM   39  HG1 ABA A   3       0.936   1.588   8.946  1.00  0.00           H  
HETATM   40  HG3 ABA A   3       0.123   1.136   7.448  1.00  0.00           H  
HETATM   41  HG2 ABA A   3       0.953  -0.091   8.405  1.00  0.00           H  
TER     310      ASN A  21                                                      
ATOM    311  N   PHE B   1       6.982  -2.147 -10.612  1.00  0.00           N  
ATOM    312  CA  PHE B   1       7.778  -1.058 -11.246  1.00  0.00           C  
ATOM    313  C   PHE B   1       9.163  -0.999 -10.599  1.00  0.00           C  
ATOM    314  O   PHE B   1      10.175  -1.047 -11.270  1.00  0.00           O  
ATOM    315  CB  PHE B   1       7.061   0.280 -11.048  1.00  0.00           C  
ATOM    316  CG  PHE B   1       7.092   1.066 -12.337  1.00  0.00           C  
ATOM    317  CD1 PHE B   1       8.309   1.556 -12.829  1.00  0.00           C  
ATOM    318  CD2 PHE B   1       5.904   1.307 -13.041  1.00  0.00           C  
ATOM    319  CE1 PHE B   1       8.339   2.287 -14.025  1.00  0.00           C  
ATOM    320  CE2 PHE B   1       5.935   2.038 -14.237  1.00  0.00           C  
TER     780      THR B  30                                                      
ENDMDL                                                                          
CONECT   12   29                                                                
CONECT   29   12   30   35                                                      
CONECT   30   29   31   33   36                                                 
CONECT   31   30   32   42                                                      
CONECT   32   31                                                                
CONECT   33   30   34   37   38                                                 
CONECT   34   33   39   40   41                                                 
CONECT   35   29                                                                
CONECT   36   30                                                                
CONECT   37   33                                                                
CONECT   38   33                                                                
CONECT   39   34                                                                
CONECT   40   34                                                                
CONECT   41   34                                                                
CONECT   42   31                                                                
CONECT   79  143                                                                
CONECT   89  421                                                                
CONECT  143   79                                                                
CONECT  290  596                                                                
CONECT  421   89                                                                
CONECT  596  290                                                                
MASTER      181    0    1    4    0    0    0    611670   30   21    5          
END                                                                             
