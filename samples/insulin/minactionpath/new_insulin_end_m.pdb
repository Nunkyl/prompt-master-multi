HEADER    HORMONE                                 05-SEP-07   2JUV              
TITLE     ABAA3-DKP-INSULIN                                                     
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: INSULIN A CHAIN;                                           
COMPND   3 CHAIN: A;                                                            
COMPND   4 ENGINEERED: YES;                                                     
COMPND   5 MUTATION: YES;                                                       
COMPND   6 MOL_ID: 2;                                                           
COMPND   7 MOLECULE: INSULIN B CHAIN;                                           
COMPND   8 CHAIN: B;                                                            
COMPND   9 ENGINEERED: YES;                                                     
COMPND  10 MUTATION: YES                                                        
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 SYNTHETIC: YES;                                                      
SOURCE   3 MOL_ID: 2;                                                           
SOURCE   4 SYNTHETIC: YES                                                       
KEYWDS    INSULIN, NMR, ABA, CARBOHYDRATE METABOLISM, CLEAVAGE ON               
KEYWDS   2 PAIR OF BASIC RESIDUES, DIABETES MELLITUS, DISEASE MUTATION          
KEYWDS   3 GLUCOSE METABOLISM, HORMONE, PHARMACEUTICAL, SECRETED                
EXPDTA    SOLUTION NMR                                                          
AUTHOR    K.HUANG,S.CHAN,Q.HUA,Y.CHU,R.WANG,B.KLAPROTH,W.JIA,                   
AUTHOR   2 J.WHITTAKER,P.DE MEYTS,S.H.NAKAGAWA,D.F.STEINER,                     
AUTHOR   3 P.G.KATSOYANNIS,M.A.WEISS                                            
REVDAT   3   24-FEB-09 2JUV    1       VERSN                                    
REVDAT   2   11-DEC-07 2JUV    1       JRNL                                     
REVDAT   1   16-OCT-07 2JUV    0                                                
JRNL        AUTH   K.HUANG,S.J.CHAN,Q.X.HUA,Y.C.CHU,R.Y.WANG,                   
JRNL        AUTH 2 B.KLAPROTH,W.JIA,J.WHITTAKER,P.DE MEYTS,                     
JRNL        AUTH 3 S.H.NAKAGAWA,D.F.STEINER,P.G.KATSOYANNIS,M.A.WEISS           
JRNL        TITL   THE A-CHAIN OF INSULIN CONTACTS THE INSERT DOMAIN            
JRNL        TITL 2 OF THE INSULIN RECEPTOR: PHOTO-CROSS-LINKING AND             
JRNL        TITL 3 MUTAGENESIS OF A DIABETES-RELATED CREVICE.                   
JRNL        REF    J.BIOL.CHEM.                  V. 282 35337 2007              
JRNL        REFN                   ISSN 0021-9258                               
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : CNS, X-PLOR 3.1                                      
REMARK   3   AUTHORS     : ACCELRYS (CNS), BRUNGER, A.T. ET AL. (X-PLO          
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: NULL                                      
REMARK   4                                                                      
REMARK   4 2JUV COMPLIES WITH FORMAT V. 3.15, 01-DEC-08                         
REMARK 100                                                                      
REMARK 100 THIS ENTRY HAS BEEN PROCESSED BY RCSB ON 20-SEP-07.                  
REMARK 100 THE RCSB ID CODE IS RCSB100319.                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 308; 315; 308                      
REMARK 210  PH                             : 7.0; 8.0; 1.9                      
REMARK 210  IONIC STRENGTH                 : NULL; NULL; NULL                   
REMARK 210  PRESSURE                       : AMBIENT; AMBIENT; AMBIENT          
REMARK 210  SAMPLE CONTENTS                : 1 MM ABAA3-DKP-INSULIN, 9          
REMARK 210                                   H2O/10% D2O; 1 MM ABAA3-D          
REMARK 210                                   INSULIN, 100% D2O; 1 MM A          
REMARK 210                                   DKP-INSULIN, 100% D2O; 1           
REMARK 210                                   ABAA3-DKP-INSULIN, 20% D-          
REMARK 210                                   ACETIC ACID/80% H2O; 1 MM          
REMARK 210                                   ABAA3-DKP-INSULIN, 20%D-A          
REMARK 210                                   ACID/80% D2O                       
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : 2D 1H-1H COSY, 2D 1H-1H            
REMARK 210                                   TOCSY, 2D 1H-1H NOESY, 2D          
REMARK 210                                   COSY                               
REMARK 210  SPECTROMETER FIELD STRENGTH    : 600 MHZ, 800 MHZ                   
REMARK 210  SPECTROMETER MODEL             : INOVA, DMX                         
REMARK 210  SPECTROMETER MANUFACTURER      : VARIAN, BRUKER                     
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : NULL                               
REMARK 210   METHOD USED                   : DGSA-DISTANCE GEOMETRY,            
REMARK 210                                   SIMULATED ANNEALING                
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 50                                 
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 15                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : STRUCTURES WITH THE LOWES          
REMARK 210                                   ENERGY                             
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : 1                   
REMARK 210                                                                      
REMARK 210 REMARK: NULL                                                         
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 THE COORDINATES IN THIS ENTRY WERE GENERATED FROM SOLUTION           
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 215 THESE RECORDS ARE MEANINGLESS.                                       
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500 CHOLOGY: RAMACHANDRAN REVISITED. STRUCTURE 4, 1395 - 1400            
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  1 SER A   9     -114.32   -123.75                                   
REMARK 500  1 VAL B   2     -141.12   -138.03                                   
REMARK 500  1 ASN B   3       61.78   -112.61                                   
REMARK 500  1 CYS B   7      140.88    157.64                                   
REMARK 500  1 CYS B  19      -71.06   -100.28                                   
REMARK 500  1 TYR B  26      109.04     54.47                                   
REMARK 500  1 THR B  27     -169.41   -109.25                                   
REMARK 500  1 LYS B  28      -57.92   -168.22                                   
REMARK 500  2 SER A   9     -139.13   -117.55                                   
REMARK 500  2 ASN B   3       60.04   -175.29                                   
REMARK 500  2 CYS B   7      138.24    159.38                                   
REMARK 500  2 CYS B  19      -64.53    -92.09                                   
REMARK 500  2 PRO B  29       49.78    -81.10                                   
REMARK 500  3 SER A   9     -130.01   -119.52                                   
REMARK 500  3 VAL B   2     -161.37   -106.07                                   
REMARK 500  3 ASN B   3       63.47   -112.70                                   
REMARK 500  3 CYS B   7      149.57    158.84                                   
REMARK 500  3 CYS B  19      -75.83   -106.95                                   
REMARK 500  3 LYS B  28      -60.09   -154.71                                   
REMARK 500  4 SER A   9     -122.34   -130.61                                   
REMARK 500  4 ASN B   3       55.55   -110.33                                   
REMARK 500  4 CYS B   7      138.36    162.09                                   
REMARK 500  4 CYS B  19      -67.83    -95.41                                   
REMARK 500  5 SER A   9     -139.07   -123.29                                   
REMARK 500  5 CYS B   7      142.31    163.95                                   
REMARK 500  5 CYS B  19      -70.55   -132.71                                   
REMARK 500  6 SER A   9     -101.15   -119.63                                   
REMARK 500  6 ASN B   3       64.64   -101.25                                   
REMARK 500  6 HIS B   5     -166.33    -69.26                                   
REMARK 500  6 CYS B   7      155.68    169.21                                   
REMARK 500  6 CYS B  19      -76.56   -130.40                                   
REMARK 500  6 PRO B  29      -90.55    -80.06                                   
REMARK 500  7 SER A   9      -98.36   -121.57                                   
REMARK 500  7 CYS B   7      135.47    159.89                                   
REMARK 500  7 TYR B  26       96.70     59.54                                   
REMARK 500  8 SER A   9     -105.68   -127.05                                   
REMARK 500  8 VAL B   2      -93.62   -130.65                                   
REMARK 500  8 ASN B   3       75.27     70.46                                   
REMARK 500  8 HIS B   5     -179.14    -67.37                                   
REMARK 500  8 CYS B   7      153.24    166.95                                   
REMARK 500  8 CYS B  19      -70.44   -129.34                                   
REMARK 500  8 PHE B  25       67.83   -115.06                                   
REMARK 500  9 SER A   9     -104.39   -145.79                                   
REMARK 500  9 LEU A  13       -8.10    -56.66                                   
REMARK 500  9 ASN B   3       57.12   -154.26                                   
REMARK 500  9 CYS B   7      135.15    158.42                                   
REMARK 500  9 CYS B  19      -61.26   -147.35                                   
REMARK 500 10 SER A   9     -107.66   -110.24                                   
REMARK 500 10 CYS B   7      134.07    144.48                                   
REMARK 500 10 LYS B  28      -60.28   -158.57                                   
REMARK 500 11 SER A   9     -110.88   -124.34                                   
REMARK 500 11 LEU A  13       -8.60    -59.31                                   
REMARK 500 11 ASN B   3       57.97   -160.51                                   
REMARK 500 11 CYS B   7      143.18    163.48                                   
REMARK 500 11 LYS B  28      -59.70   -156.02                                   
REMARK 500 12 SER A   9      -97.49   -146.39                                   
REMARK 500 12 ASN B   3       54.49   -174.12                                   
REMARK 500 12 CYS B   7      145.46    162.03                                   
REMARK 500 12 CYS B  19      -53.50   -137.45                                   
REMARK 500 13 SER A   9     -125.75   -130.81                                   
REMARK 500 13 CYS A  20      174.89    -52.18                                   
REMARK 500 13 VAL B   2      -84.46   -159.70                                   
REMARK 500 13 ASN B   3       79.67     68.14                                   
REMARK 500 13 CYS B   7      137.49    157.95                                   
REMARK 500 13 PHE B  25       69.81   -115.36                                   
REMARK 500 14 SER A   9     -129.25   -121.42                                   
REMARK 500 14 VAL B   2     -160.66   -160.12                                   
REMARK 500 14 HIS B   5     -161.52    -67.70                                   
REMARK 500 14 CYS B   7      152.70    170.08                                   
REMARK 500 14 CYS B  19      -76.27   -133.67                                   
REMARK 500 15 CYS A   7      -32.30   -144.69                                   
REMARK 500 15 SER A   9     -138.48    -99.66                                   
REMARK 500 15 LEU A  13       -8.03    -58.23                                   
REMARK 500 15 ASN B   3       58.55   -144.33                                   
REMARK 500 15 CYS B  19      -71.19   -137.29                                   
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: PLANAR GROUPS                                              
REMARK 500                                                                      
REMARK 500 PLANAR GROUPS IN THE FOLLOWING RESIDUES HAVE A TOTAL                 
REMARK 500 RMS DISTANCE OF ALL ATOMS FROM THE BEST-FIT PLANE                    
REMARK 500 BY MORE THAN AN EXPECTED VALUE OF 6*RMSD, WITH AN                    
REMARK 500 RMSD 0.02 ANGSTROMS, OR AT LEAST ONE ATOM HAS                        
REMARK 500 AN RMSD GREATER THAN THIS VALUE                                      
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        RMS     TYPE                                    
REMARK 500  1 ARG B  22         0.10    SIDE_CHAIN                              
REMARK 500  2 ARG B  22         0.20    SIDE_CHAIN                              
REMARK 500  3 ARG B  22         0.23    SIDE_CHAIN                              
REMARK 500  4 ARG B  22         0.28    SIDE_CHAIN                              
REMARK 500  5 ARG B  22         0.32    SIDE_CHAIN                              
REMARK 500  7 ARG B  22         0.30    SIDE_CHAIN                              
REMARK 500  8 ARG B  22         0.21    SIDE_CHAIN                              
REMARK 500  9 ARG B  22         0.18    SIDE_CHAIN                              
REMARK 500 10 ARG B  22         0.23    SIDE_CHAIN                              
REMARK 500 11 ARG B  22         0.21    SIDE_CHAIN                              
REMARK 500 12 ARG B  22         0.19    SIDE_CHAIN                              
REMARK 500 13 ARG B  22         0.14    SIDE_CHAIN                              
REMARK 500 14 ARG B  22         0.18    SIDE_CHAIN                              
REMARK 500 15 ARG B  22         0.30    SIDE_CHAIN                              
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 900                                                                      
REMARK 900 RELATED ENTRIES                                                      
REMARK 900 RELATED ID: 2JUM   RELATED DB: PDB                                   
REMARK 900 RELATED ID: 2JUU   RELATED DB: PDB                                   
DBREF  2JUV A    1    21  UNP    P01308   INS_HUMAN       90    110             
DBREF  2JUV B    1    30  UNP    P01308   INS_HUMAN       25     54             
SEQADV 2JUV ABA A    3  UNP  P01308    VAL    92 ENGINEERED                     
SEQADV 2JUV ASP B   10  UNP  P01308    HIS    34 ENGINEERED                     
SEQADV 2JUV LYS B   28  UNP  P01308    PRO    52 ENGINEERED                     
SEQADV 2JUV PRO B   29  UNP  P01308    LYS    53 ENGINEERED                     
SEQRES   1 A   21  GLY ILE ABA GLU GLN CYS CYS THR SER ILE CYS SER LEU          
SEQRES   2 A   21  TYR GLN LEU GLU ASN TYR CYS ASN                              
SEQRES   1 B   30  PHE VAL ASN GLN HIS LEU CYS GLY SER ASP LEU VAL GLU          
SEQRES   2 B   30  ALA LEU TYR LEU VAL CYS GLY GLU ARG GLY PHE PHE TYR          
SEQRES   3 B   30  THR LYS PRO THR                                              
MODRES 2JUV ABA A    3  ALA  ALPHA-AMINOBUTYRIC ACID                            
HET    ABA  A   3      13                                                       
HETNAM     ABA ALPHA-AMINOBUTYRIC ACID                                          
FORMUL   1  ABA    C4 H9 N O2                                                   
HELIX    1   1 GLY A    1  THR A    8  1                                   8    
HELIX    2   2 LEU A   16  CYS A   20  5                                   5    
HELIX    3   3 GLY B    8  CYS B   19  1                                  12    
HELIX    4   4 GLY B   20  GLY B   23  5                                   4    
SSBOND   1 CYS A    6    CYS A   11                          1555   1555        
SSBOND   2 CYS A    7    CYS B    7                          1555   1555        
SSBOND   3 CYS A   20    CYS B   19                          1555   1555        
LINK         C   ILE A   2                 N   ABA A   3     1555   1555        
LINK         C   ABA A   3                 N   GLU A   4     1555   1555        
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
MODEL       15                                                                  
ATOM      2  CA  GLY A   1       0.197  -3.734   9.600  1.00  0.00           C  
ATOM     11  CA  ILE A   2       0.472  -3.112   5.877  1.00  0.00           C  
ATOM     30  CA  ALA A   3       2.558   0.066   6.309  1.00  0.00           C  
ATOM     43  CA  GLU A   4       5.330  -1.479   8.452  1.00  0.00           C  
ATOM     58  CA  GLN A   5       5.595  -4.255   5.832  1.00  0.00           C  
ATOM     75  CA  CYS A   6       4.815  -1.896   2.945  1.00  0.00           C  
ATOM     85  CA  CYS A   7       7.054   0.727   4.617  1.00  0.00           C  
ATOM     95  CA  THR A   8      10.080  -1.074   6.143  1.00  0.00           C  
ATOM    109  CA  SER A   9      10.332  -3.812   3.510  1.00  0.00           C  
ATOM    120  CA  ILE A  10       9.015  -3.534  -0.068  1.00  0.00           C  
ATOM    139  CA  CYS A  11       5.484  -3.556  -1.528  1.00  0.00           C  
ATOM    149  CA  SER A  12       4.625  -3.750  -5.240  1.00  0.00           C  
ATOM    160  CA  LEU A  13       1.723  -1.786  -6.714  1.00  0.00           C  
ATOM    179  CA  TYR A  14      -0.074  -5.150  -6.991  1.00  0.00           C  
ATOM    200  CA  GLN A  15       0.714  -6.238  -3.496  1.00  0.00           C  
ATOM    217  CA  LEU A  16      -0.643  -2.822  -2.582  1.00  0.00           C  
ATOM    236  CA  GLU A  17      -3.489  -3.414  -4.983  1.00  0.00           C  
ATOM    251  CA  ASN A  18      -4.451  -6.442  -2.851  1.00  0.00           C  
ATOM    265  CA  TYR A  19      -5.176  -4.111   0.090  1.00  0.00           C  
ATOM    286  CA  CYS A  20      -8.219  -2.578  -1.638  1.00  0.00           C  
ATOM    296  CA  ASN A  21     -11.853  -3.615  -1.095  1.00  0.00           C  
TER     310      ASN A  21                                                      
ATOM    312  CA  PHE B   1       7.778  -1.058 -11.246  1.00  0.00           C  
ATOM    334  CA  VAL B   2      10.536  -0.837  -8.607  1.00  0.00           C  
ATOM    350  CA  ASN B   3      11.419  -2.437  -5.243  1.00  0.00           C  
ATOM    364  CA  GLN B   4      11.370   0.244  -2.526  1.00  0.00           C  
ATOM    381  CA  HIS B   5       9.385   0.772   0.706  1.00  0.00           C  
ATOM    398  CA  LEU B   6       6.189   2.848   1.123  1.00  0.00           C  
ATOM    417  CA  CYS B   7       5.152   5.185   3.937  1.00  0.00           C  
ATOM    427  CA  GLY B   8       2.790   8.168   4.346  1.00  0.00           C  
ATOM    434  CA  SER B   9       1.289   9.570   1.124  1.00  0.00           C  
ATOM    445  CA  ASP B  10       3.612   7.202  -0.750  1.00  0.00           C  
ATOM    457  CA  LEU B  11       1.491   4.353   0.631  1.00  0.00           C  
ATOM    476  CA  VAL B  12      -1.871   5.968  -0.209  1.00  0.00           C  
ATOM    492  CA  GLU B  13      -0.662   7.509  -3.476  1.00  0.00           C  
ATOM    507  CA  ALA B  14      -0.038   3.835  -4.297  1.00  0.00           C  
ATOM    517  CA  LEU B  15      -3.095   2.305  -2.609  1.00  0.00           C  
ATOM    536  CA  TYR B  16      -5.182   4.981  -4.342  1.00  0.00           C  
ATOM    557  CA  LEU B  17      -3.404   4.262  -7.652  1.00  0.00           C  
ATOM    576  CA  VAL B  18      -3.209   0.466  -7.300  1.00  0.00           C  
ATOM    592  CA  CYS B  19      -6.979   0.719  -7.087  1.00  0.00           C  
ATOM    602  CA  GLY B  20      -8.532   4.181  -6.587  1.00  0.00           C  
ATOM    609  CA  GLU B  21     -12.184   4.436  -5.475  1.00  0.00           C  
ATOM    624  CA  ARG B  22     -11.914   0.796  -4.333  1.00  0.00           C  
ATOM    648  CA  GLY B  23     -10.673   2.208  -1.005  1.00  0.00           C  
ATOM    655  CA  PHE B  24      -7.595   0.741   0.711  1.00  0.00           C  
ATOM    675  CA  PHE B  25      -7.178  -0.900   4.142  1.00  0.00           C  
ATOM    695  CA  TYR B  26      -4.683   1.570   5.665  1.00  0.00           C  
ATOM    716  CA  THR B  27      -4.319   0.454   9.306  1.00  0.00           C  
ATOM    730  CA  LYS B  28      -1.618   1.678  11.722  1.00  0.00           C  
ATOM    752  CA  PRO B  29      -1.990  -0.452  14.841  1.00  0.00           C  
ATOM    766  CA  THR B  30      -1.928  -3.991  13.383  1.00  0.00           C  
TER     780      THR B  30                                                      
ENDMDL                                                                          
CONECT   12   29                                                                
CONECT   29   12   30   35                                                      
CONECT   30   29   31   33   36                                                 
CONECT   31   30   32   42                                                      
CONECT   32   31                                                                
CONECT   33   30   34   37   38                                                 
CONECT   34   33   39   40   41                                                 
CONECT   35   29                                                                
CONECT   36   30                                                                
CONECT   37   33                                                                
CONECT   38   33                                                                
CONECT   39   34                                                                
CONECT   40   34                                                                
CONECT   41   34                                                                
CONECT   42   31                                                                
CONECT   79  143                                                                
CONECT   89  421                                                                
CONECT  143   79                                                                
CONECT  290  596                                                                
CONECT  421   89                                                                
CONECT  596  290                                                                
MASTER      181    0    1    4    0    0    0    611670   30   21    5          
END                                                                             
