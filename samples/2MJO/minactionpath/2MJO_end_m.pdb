HEADER    MEMBRANE PROTEIN                        15-JAN-14   2MJO              
TITLE     NMR STRUCTURE OF P75 TRANSMEMBRANE DOMAIN C257A MUTANT IN DP          
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: TUMOR NECROSIS FACTOR RECEPTOR SUPERFAMILY MEMBER          
COMPND   3 CHAIN: A, B;                                                         
COMPND   4 FRAGMENT: UNP RESIDUES 245-284;                                      
COMPND   5 SYNONYM: GP80-LNGFR, LOW AFFINITY NEUROTROPHIN RECEPTOR P75          
COMPND   6 AFFINITY NERVE GROWTH FACTOR RECEPTOR, NGF RECEPTOR, P75 IC          
COMPND   7 ENGINEERED: YES;                                                     
COMPND   8 MUTATION: YES                                                        
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 ORGANISM_SCIENTIFIC: RATTUS NORVEGICUS;                              
SOURCE   3 ORGANISM_COMMON: BROWN RAT,RAT,RATS;                                 
SOURCE   4 ORGANISM_TAXID: 10116;                                               
SOURCE   5 GENE: NGFR, TNFRSF16;                                                
SOURCE   6 EXPRESSION_SYSTEM: ESCHERICHIA COLI;                                 
SOURCE   7 EXPRESSION_SYSTEM_TAXID: 562;                                        
SOURCE   8 EXPRESSION_SYSTEM_VECTOR_TYPE: PLASMID;                              
SOURCE   9 EXPRESSION_SYSTEM_PLASMID: PGEMEX-1                                  
KEYWDS    P75, DIMER, TRANSMEMBRANE, C257A MUTATION, MEMBRANE PROTEIN           
EXPDTA    SOLUTION NMR                                                          
AUTHOR    K.NADEZHDIN,A.ARSENIEV,S.GONCHARUK,K.MINEEV                           
REVDAT   1   28-JAN-15 2MJO    0                                                
JRNL        AUTH   K.NADEZHDIN,A.ARSENIEV,S.GONCHARUK,K.MINEEV,M.VILAR          
JRNL        AUTH 2 I.GARCA-CARPIO,E.FERNANDEZ                                   
JRNL        TITL   STRUCTURE OF DIMERIC P75 NEUROTROPHIN RECEPTOR TRAN          
JRNL        TITL 2 DOMAIN PROVIDES INSIGHT ON NEUROTROPHIN SIGNALING            
JRNL        REF    TO BE PUBLISHED                                              
JRNL        REFN                                                                
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : CYANA                                                
REMARK   3   AUTHORS     : NULL                                                 
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: NULL                                      
REMARK   4                                                                      
REMARK   4 2MJO COMPLIES WITH FORMAT V. 3.30, 13-JUL-11                         
REMARK 100                                                                      
REMARK 100 THIS ENTRY HAS BEEN PROCESSED BY RCSB ON 16-JAN-14.                  
REMARK 100 THE RCSB ID CODE IS RCSB103694.                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 318                                
REMARK 210  PH                             : 5.9                                
REMARK 210  IONIC STRENGTH                 : NULL                               
REMARK 210  PRESSURE                       : AMBIENT                            
REMARK 210  SAMPLE CONTENTS                : 1.5 MM [U-100% 13C; U-100          
REMARK 210                                   P75-TM-C257A, 30 MM [U-98          
REMARK 210                                   DPC, 20 MM SODIUM PHOSPHA          
REMARK 210                                   H2O/5% D2O                         
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : 2D 1H-15N HSQC; 2D 1H-13C          
REMARK 210                                   3D HNCO; 3D HNCA; 3D 1H-1          
REMARK 210                                   NOESY; 3D 1H-13C NOESY; 2          
REMARK 210                                   HSQC AROMATIC; 3D HN(CO)C          
REMARK 210  SPECTROMETER FIELD STRENGTH    : 800 MHZ; 600 MHZ                   
REMARK 210  SPECTROMETER MODEL             : AVANCE                             
REMARK 210  SPECTROMETER MANUFACTURER      : BRUKER                             
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : TOPSPIN, CYANA, TALOS-N,           
REMARK 210                                   MOLMOL, QMDD                       
REMARK 210   METHOD USED                   : SIMULATED ANNEALING                
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 10                                 
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 10                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : TARGET FUNCTION                    
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : 1                   
REMARK 210                                                                      
REMARK 210 REMARK: NULL                                                         
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 THE COORDINATES IN THIS ENTRY WERE GENERATED FROM SOLUTION           
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 215 THESE RECORDS ARE MEANINGLESS.                                       
REMARK 300                                                                      
REMARK 300 BIOMOLECULE: 1                                                       
REMARK 300 SEE REMARK 350 FOR THE AUTHOR PROVIDED AND/OR PROGRAM                
REMARK 300 GENERATED ASSEMBLY INFORMATION FOR THE STRUCTURE IN                  
REMARK 300 THIS ENTRY. THE REMARK MAY ALSO PROVIDE INFORMATION ON               
REMARK 300 BURIED SURFACE AREA.                                                 
REMARK 350                                                                      
REMARK 350 COORDINATES FOR A COMPLETE MULTIMER REPRESENTING THE KNOWN           
REMARK 350 BIOLOGICALLY SIGNIFICANT OLIGOMERIZATION STATE OF THE                
REMARK 350 MOLECULE CAN BE GENERATED BY APPLYING BIOMT TRANSFORMATIONS          
REMARK 350 GIVEN BELOW.  BOTH NON-CRYSTALLOGRAPHIC AND                          
REMARK 350 CRYSTALLOGRAPHIC OPERATIONS ARE GIVEN.                               
REMARK 350                                                                      
REMARK 350 BIOMOLECULE: 1                                                       
REMARK 350 AUTHOR DETERMINED BIOLOGICAL UNIT: DIMERIC                           
REMARK 350 APPLY THE FOLLOWING TO CHAINS: A, B                                  
REMARK 350   BIOMT1   1  1.000000  0.000000  0.000000        0.00000            
REMARK 350   BIOMT2   1  0.000000  1.000000  0.000000        0.00000            
REMARK 350   BIOMT3   1  0.000000  0.000000  1.000000        0.00000            
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500 CHOLOGY: RAMACHANDRAN REVISITED. STRUCTURE 4, 1395 - 1400            
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  2 THR A   2      -68.26   -105.88                                   
REMARK 500  2 ASN A  39      -68.45    -95.24                                   
REMARK 500  2 THR B 102      -68.63   -105.14                                   
REMARK 500  2 TYR B 127      -71.04    -48.45                                   
REMARK 500  2 ASN B 139      -68.36    -95.15                                   
REMARK 500  3 THR A   5      -71.51    -72.53                                   
REMARK 500  3 SER A  36       72.88     50.60                                   
REMARK 500  3 THR B 105      -71.04    -70.18                                   
REMARK 500  3 TYR B 127      -70.31    -47.41                                   
REMARK 500  3 SER B 136       85.55     58.52                                   
REMARK 500  5 THR A   6      -75.65    -44.87                                   
REMARK 500  5 THR B 106      -75.79    -44.91                                   
REMARK 500  5 TYR B 127      -70.06    -48.27                                   
REMARK 500  7 THR A   6      171.12    177.98                                   
REMARK 500  7 ASN A   8      -50.39   -139.17                                   
REMARK 500  7 THR B 106      174.35     68.09                                   
REMARK 500  7 ASN B 108      -44.48   -165.98                                   
REMARK 500  8 VAL B 112      -76.48    -67.77                                   
REMARK 500  9 ASN A   8       30.18   -172.16                                   
REMARK 500  9 ASN B 108       33.68   -179.40                                   
REMARK 500 10 TYR B 127      -70.29    -46.95                                   
REMARK 500 10 ASN B 139      -65.40    -90.06                                   
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 900                                                                      
REMARK 900 RELATED ENTRIES                                                      
REMARK 900 RELATED ID: 2MIC   RELATED DB: PDB                                   
REMARK 900 RELATED ID: 19741   RELATED DB: BMRB                                 
DBREF  2MJO A    2    41  UNP    P07174   TNR16_RAT      245    284             
DBREF  2MJO B  102   141  UNP    P07174   TNR16_RAT      245    284             
SEQADV 2MJO MET A    1  UNP  P07174          NaN INITIATING METHIONINE          
SEQADV 2MJO ALA A   14  UNP  P07174    CYS   257 ENGINEERED MUTATION            
SEQADV 2MJO SER A   36  UNP  P07174    CYS   279 ENGINEERED MUTATION            
SEQADV 2MJO MET B  101  UNP  P07174          NaN INITIATING METHIONINE          
SEQADV 2MJO ALA B  114  UNP  P07174    CYS   257 ENGINEERED MUTATION            
SEQADV 2MJO SER B  136  UNP  P07174    CYS   279 ENGINEERED MUTATION            
SEQRES   1 A   41  MET THR ARG GLY THR THR ASP ASN LEU ILE PRO VAL TYR          
SEQRES   2 A   41  ALA SER ILE LEU ALA ALA VAL VAL VAL GLY LEU VAL ALA          
SEQRES   3 A   41  TYR ILE ALA PHE LYS ARG TRP ASN SER SER LYS GLN ASN          
SEQRES   4 A   41  LYS GLN                                                      
SEQRES   1 B   41  MET THR ARG GLY THR THR ASP ASN LEU ILE PRO VAL TYR          
SEQRES   2 B   41  ALA SER ILE LEU ALA ALA VAL VAL VAL GLY LEU VAL ALA          
SEQRES   3 B   41  TYR ILE ALA PHE LYS ARG TRP ASN SER SER LYS GLN ASN          
SEQRES   4 B   41  LYS GLN                                                      
HELIX    1   1 LEU A    9  ASN A   34  1                                  26    
HELIX    2   2 LEU B  109  ASN B  134  1                                  26    
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
MODEL       10                                                                  
ATOM      2  CA  MET A   1       0.359  11.406  -1.619  1.00  0.00           C  
ATOM     19  CA  THR A   2       3.558   9.394  -1.177  1.00  0.00           C  
ATOM     33  CA  ARG A   3       4.324   5.822  -0.120  1.00  0.00           C  
ATOM     57  CA  GLY A   4       6.408   2.730  -0.809  1.00  0.00           C  
ATOM     64  CA  THR A   5      10.060   3.651  -1.284  1.00  0.00           C  
ATOM     78  CA  THR A   6      10.474   7.378  -1.914  1.00  0.00           C  
ATOM     92  CA  ASP A   7      14.213   6.688  -1.881  1.00  0.00           C  
ATOM    104  CA  ASN A   8      13.732   5.275  -5.380  1.00  0.00           C  
ATOM    118  CA  LEU A   9      10.838   7.501  -6.448  1.00  0.00           C  
ATOM    137  CA  ILE A  10      13.282  10.353  -7.048  1.00  0.00           C  
ATOM    156  CA  PRO A  11      13.019   9.816 -10.827  1.00  0.00           C  
ATOM    170  CA  VAL A  12       9.302   9.023 -10.700  1.00  0.00           C  
ATOM    186  CA  TYR A  13       8.255  12.415  -9.340  1.00  0.00           C  
ATOM    207  CA  ALA A  14      11.152  14.157 -11.083  1.00  0.00           C  
ATOM    217  CA  SER A  15       9.992  13.036 -14.528  1.00  0.00           C  
ATOM    228  CA  ILE A  16       6.286  13.534 -13.838  1.00  0.00           C  
ATOM    247  CA  LEU A  17       7.041  17.042 -12.576  1.00  0.00           C  
ATOM    266  CA  ALA A  18       9.004  17.786 -15.748  1.00  0.00           C  
ATOM    276  CA  ALA A  19       6.259  16.777 -18.179  1.00  0.00           C  
ATOM    286  CA  VAL A  20       3.784  18.829 -16.146  1.00  0.00           C  
ATOM    302  CA  VAL A  21       5.919  21.956 -15.782  1.00  0.00           C  
ATOM    318  CA  VAL A  22       6.905  21.753 -19.449  1.00  0.00           C  
ATOM    334  CA  GLY A  23       3.301  21.294 -20.530  1.00  0.00           C  
ATOM    341  CA  LEU A  24       2.212  24.442 -18.709  1.00  0.00           C  
ATOM    360  CA  VAL A  25       5.211  26.587 -19.640  1.00  0.00           C  
ATOM    376  CA  ALA A  26       5.407  25.397 -23.245  1.00  0.00           C  
ATOM    386  CA  TYR A  27       1.653  25.653 -23.805  1.00  0.00           C  
ATOM    407  CA  ILE A  28       1.212  29.088 -22.235  1.00  0.00           C  
ATOM    426  CA  ALA A  29       4.488  30.365 -23.684  1.00  0.00           C  
ATOM    436  CA  PHE A  30       3.651  29.253 -27.224  1.00  0.00           C  
ATOM    456  CA  LYS A  31       0.139  30.541 -26.533  1.00  0.00           C  
ATOM    478  CA  ARG A  32       1.226  34.139 -25.948  1.00  0.00           C  
ATOM    502  CA  TRP A  33       4.134  33.833 -28.380  1.00  0.00           C  
ATOM    526  CA  ASN A  34       1.787  32.786 -31.186  1.00  0.00           C  
ATOM    540  CA  SER A  35       0.470  36.355 -31.170  1.00  0.00           C  
ATOM    551  CA  SER A  36       4.008  37.749 -31.142  1.00  0.00           C  
ATOM    562  CA  LYS A  37       6.248  37.419 -34.199  1.00  0.00           C  
ATOM    584  CA  GLN A  38      10.011  37.962 -34.323  1.00  0.00           C  
ATOM    601  CA  ASN A  39      12.831  38.229 -36.861  1.00  0.00           C  
ATOM    615  CA  LYS A  40      15.771  36.356 -35.339  1.00  0.00           C  
ATOM    637  CA  GLN A  41      15.999  34.782 -31.883  1.00  0.00           C  
TER     653      GLN A  41                                                      
ATOM    655  CA  MET B 101      -0.192   3.185  -9.599  1.00  0.00           C  
ATOM    672  CA  THR B 102      -3.517   3.889  -7.888  1.00  0.00           C  
ATOM    686  CA  ARG B 103      -4.717   3.909  -4.280  1.00  0.00           C  
ATOM    710  CA  GLY B 104      -6.960   5.609  -1.737  1.00  0.00           C  
ATOM    717  CA  THR B 105     -10.273   6.595  -3.308  1.00  0.00           C  
ATOM    731  CA  THR B 106     -10.044   6.790  -7.099  1.00  0.00           C  
ATOM    745  CA  ASP B 107     -13.786   7.458  -7.013  1.00  0.00           C  
ATOM    757  CA  ASN B 108     -12.909  10.997  -5.926  1.00  0.00           C  
ATOM    771  CA  LEU B 109      -9.547  11.288  -7.681  1.00  0.00           C  
ATOM    790  CA  ILE B 110     -11.232  11.863 -11.040  1.00  0.00           C  
ATOM    809  CA  PRO B 111      -9.935  15.446 -11.397  1.00  0.00           C  
ATOM    823  CA  VAL B 112      -6.562  14.640  -9.833  1.00  0.00           C  
ATOM    839  CA  TYR B 113      -5.499  12.133 -12.486  1.00  0.00           C  
ATOM    860  CA  ALA B 114      -7.513  13.935 -15.162  1.00  0.00           C  
ATOM    870  CA  SER B 115      -5.573  17.175 -14.711  1.00  0.00           C  
ATOM    881  CA  ILE B 116      -2.191  15.503 -14.226  1.00  0.00           C  
ATOM    900  CA  LEU B 117      -2.795  13.487 -17.395  1.00  0.00           C  
ATOM    919  CA  ALA B 118      -3.580  16.654 -19.349  1.00  0.00           C  
ATOM    929  CA  ALA B 119      -0.325  18.324 -18.306  1.00  0.00           C  
ATOM    939  CA  VAL B 120       1.602  15.216 -19.351  1.00  0.00           C  
ATOM    955  CA  VAL B 121      -0.333  14.807 -22.599  1.00  0.00           C  
ATOM    971  CA  VAL B 122       0.110  18.461 -23.551  1.00  0.00           C  
ATOM    987  CA  GLY B 123       3.749  18.570 -22.492  1.00  0.00           C  
ATOM    994  CA  LEU B 124       4.593  15.576 -24.668  1.00  0.00           C  
ATOM   1013  CA  VAL B 125       2.674  16.693 -27.757  1.00  0.00           C  
ATOM   1029  CA  ALA B 126       3.497  20.390 -27.432  1.00  0.00           C  
ATOM   1039  CA  TYR B 127       7.189  19.823 -26.719  1.00  0.00           C  
ATOM   1060  CA  ILE B 128       7.782  17.257 -29.463  1.00  0.00           C  
ATOM   1079  CA  ALA B 129       5.521  19.091 -31.909  1.00  0.00           C  
ATOM   1089  CA  PHE B 130       7.158  22.484 -31.376  1.00  0.00           C  
ATOM   1109  CA  LYS B 131      10.451  20.582 -31.487  1.00  0.00           C  
ATOM   1131  CA  ARG B 132       9.868  19.213 -34.987  1.00  0.00           C  
ATOM   1155  CA  TRP B 133       8.099  22.393 -36.094  1.00  0.00           C  
ATOM   1179  CA  ASN B 134      11.091  24.526 -35.108  1.00  0.00           C  
ATOM   1193  CA  SER B 135      13.045  22.836 -37.900  1.00  0.00           C  
ATOM   1204  CA  SER B 136      10.157  23.348 -40.321  1.00  0.00           C  
ATOM   1215  CA  LYS B 137       9.377  26.794 -41.731  1.00  0.00           C  
ATOM   1237  CA  GLN B 138       6.200  27.858 -43.533  1.00  0.00           C  
ATOM   1254  CA  ASN B 139       4.888  30.752 -45.624  1.00  0.00           C  
ATOM   1268  CA  LYS B 140       1.294  31.300 -44.505  1.00  0.00           C  
ATOM   1290  CA  GLN B 141      -0.745  29.212 -42.066  1.00  0.00           C  
TER    1306      GLN B 141                                                      
ENDMDL                                                                          
MASTER      113    0    0    2    0    0    0    6  636    2    0    8          
END                                                                             
