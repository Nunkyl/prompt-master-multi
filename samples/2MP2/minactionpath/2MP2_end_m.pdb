HEADER    PROTEIN BINDING                         09-MAY-14   2MP2              
TITLE     SOLUTION STRUCTURE OF SUMO DIMER IN COMPLEX WITH SIM2-3 FROM          
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: SMALL UBIQUITIN-RELATED MODIFIER 3;                        
COMPND   3 CHAIN: A;                                                            
COMPND   4 FRAGMENT: UNP RESIDUES 12-92;                                        
COMPND   5 SYNONYM: SUMO-3, SMT3 HOMOLOG 1, SUMO-2, UBIQUITIN-LIKE PRO          
COMPND   6 SMT3B, SMT3B;                                                        
COMPND   7 ENGINEERED: YES;                                                     
COMPND   8 MOL_ID: 2;                                                           
COMPND   9 MOLECULE: SMALL UBIQUITIN-RELATED MODIFIER 3;                        
COMPND  10 CHAIN: B;                                                            
COMPND  11 FRAGMENT: UNP RESIDUES 2-90;                                         
COMPND  12 SYNONYM: SUMO-3, SMT3 HOMOLOG 1, SUMO-2, UBIQUITIN-LIKE PRO          
COMPND  13 SMT3B, SMT3B;                                                        
COMPND  14 ENGINEERED: YES;                                                     
COMPND  15 MOL_ID: 3;                                                           
COMPND  16 MOLECULE: E3 UBIQUITIN-PROTEIN LIGASE RNF4;                          
COMPND  17 CHAIN: C;                                                            
COMPND  18 FRAGMENT: UNP RESIDUES 45-69;                                        
COMPND  19 SYNONYM: RING FINGER PROTEIN 4;                                      
COMPND  20 ENGINEERED: YES                                                      
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 ORGANISM_SCIENTIFIC: HOMO SAPIENS;                                   
SOURCE   3 ORGANISM_COMMON: HUMAN;                                              
SOURCE   4 ORGANISM_TAXID: 9606;                                                
SOURCE   5 GENE: SMT3B, SMT3H1, SUMO3;                                          
SOURCE   6 EXPRESSION_SYSTEM: ESCHERICHIA COLI;                                 
SOURCE   7 EXPRESSION_SYSTEM_TAXID: 469008;                                     
SOURCE   8 EXPRESSION_SYSTEM_STRAIN: BL21(DE3);                                 
SOURCE   9 EXPRESSION_SYSTEM_VECTOR_TYPE: PLASMID;                              
SOURCE  10 EXPRESSION_SYSTEM_PLASMID: PHIS-TEV-30A;                             
SOURCE  11 MOL_ID: 2;                                                           
SOURCE  12 ORGANISM_SCIENTIFIC: HOMO SAPIENS;                                   
SOURCE  13 ORGANISM_COMMON: HUMAN;                                              
SOURCE  14 ORGANISM_TAXID: 9606;                                                
SOURCE  15 GENE: SMT3B, SMT3H1, SUMO3;                                          
SOURCE  16 EXPRESSION_SYSTEM: ESCHERICHIA COLI;                                 
SOURCE  17 EXPRESSION_SYSTEM_TAXID: 469008;                                     
SOURCE  18 EXPRESSION_SYSTEM_STRAIN: BL21(DE3);                                 
SOURCE  19 EXPRESSION_SYSTEM_VECTOR_TYPE: PLASMID;                              
SOURCE  20 EXPRESSION_SYSTEM_PLASMID: PHIS-TEV-30A;                             
SOURCE  21 MOL_ID: 3;                                                           
SOURCE  22 SYNTHETIC: YES;                                                      
SOURCE  23 ORGANISM_SCIENTIFIC: MUS MUSCULUS;                                   
SOURCE  24 ORGANISM_COMMON: MOUSE;                                              
SOURCE  25 ORGANISM_TAXID: 10090                                                
KEYWDS    SUMO, DIMER, SIM, RNF4, COMPLEX, PROTEIN BINDING                      
EXPDTA    SOLUTION NMR                                                          
AUTHOR    Y.XU,A.PLECHANOVOV,P.SIMPSON,J.MARCHANT,O.LEIDECKER,K.SEBAST          
AUTHOR   2 R.T.HAY,S.J.MATTHEWS                                                 
REVDAT   2   09-JUL-14 2MP2    1       JRNL                                     
REVDAT   1   02-JUL-14 2MP2    0                                                
JRNL        AUTH   Y.XU,A.PLECHANOVOVA,P.SIMPSON,J.MARCHANT,O.LEIDECKE          
JRNL        AUTH 2 S.KRAATZ,R.T.HAY,S.J.MATTHEWS                                
JRNL        TITL   STRUCTURAL INSIGHT INTO SUMO CHAIN RECOGNITION AND           
JRNL        TITL 2 MANIPULATION BY THE UBIQUITIN LIGASE RNF4.                   
JRNL        REF    NAT COMMUN                    V.   5  4217 2014              
JRNL        REFN                   ESSN 2041-1723                               
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : CNS                                                  
REMARK   3   AUTHORS     : BRUNGER, ADAMS, CLORE, GROS, NILGES AND REA          
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: NULL                                      
REMARK   4                                                                      
REMARK   4 2MP2 COMPLIES WITH FORMAT V. 3.30, 13-JUL-11                         
REMARK 100                                                                      
REMARK 100 THIS ENTRY HAS BEEN PROCESSED BY RCSB ON 13-MAY-14.                  
REMARK 100 THE RCSB ID CODE IS RCSB103880.                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 303                                
REMARK 210  PH                             : 7.0                                
REMARK 210  IONIC STRENGTH                 : 0.1                                
REMARK 210  PRESSURE                       : AMBIENT                            
REMARK 210  SAMPLE CONTENTS                : 0.3 MM [U-99% 13C; U-99%           
REMARK 210                                   SUMO 1, 0.3 MM SUMO 2, 1.          
REMARK 210                                   RNF4, 90% H2O/10% D2O; 0.          
REMARK 210                                   SUMO 1, 0.3 MM [U-99% 13C          
REMARK 210                                   15N] SUMO 2, 1.0 MM RNF4,          
REMARK 210                                   H2O/10% D2O; 0.3 MM [U-99          
REMARK 210                                   -99% 15N] SUMO 1, 0.3 MM           
REMARK 210                                   90% H2O/10% D2O; 0.3 MM S          
REMARK 210                                   0.3 MM [U-99% 13C; U-99%           
REMARK 210                                   SUMO 2, 90% H2O/10% D2O            
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : 2D 1H-15N HSQC; 3D HNCACB          
REMARK 210                                   CBCA(CO)NH; 3D HCCH-TOCSY          
REMARK 210                                   15N NOESY                          
REMARK 210  SPECTROMETER FIELD STRENGTH    : 600 MHZ                            
REMARK 210  SPECTROMETER MODEL             : DRX                                
REMARK 210  SPECTROMETER MANUFACTURER      : BRUKER                             
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : CNS, ARIA, NMRPIPE, NMRVI          
REMARK 210                                   TOPSPIN                            
REMARK 210   METHOD USED                   : SIMULATED ANNEALING                
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 60                                 
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 10                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : STRUCTURES WITH THE LOWES          
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : 1                   
REMARK 210                                                                      
REMARK 210 REMARK: NULL                                                         
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 THE COORDINATES IN THIS ENTRY WERE GENERATED FROM SOLUTION           
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 215 THESE RECORDS ARE MEANINGLESS.                                       
REMARK 300                                                                      
REMARK 300 BIOMOLECULE: 1                                                       
REMARK 300 SEE REMARK 350 FOR THE AUTHOR PROVIDED AND/OR PROGRAM                
REMARK 300 GENERATED ASSEMBLY INFORMATION FOR THE STRUCTURE IN                  
REMARK 300 THIS ENTRY. THE REMARK MAY ALSO PROVIDE INFORMATION ON               
REMARK 300 BURIED SURFACE AREA.                                                 
REMARK 350                                                                      
REMARK 350 COORDINATES FOR A COMPLETE MULTIMER REPRESENTING THE KNOWN           
REMARK 350 BIOLOGICALLY SIGNIFICANT OLIGOMERIZATION STATE OF THE                
REMARK 350 MOLECULE CAN BE GENERATED BY APPLYING BIOMT TRANSFORMATIONS          
REMARK 350 GIVEN BELOW.  BOTH NON-CRYSTALLOGRAPHIC AND                          
REMARK 350 CRYSTALLOGRAPHIC OPERATIONS ARE GIVEN.                               
REMARK 350                                                                      
REMARK 350 BIOMOLECULE: 1                                                       
REMARK 350 AUTHOR DETERMINED BIOLOGICAL UNIT: TRIMERIC                          
REMARK 350 APPLY THE FOLLOWING TO CHAINS: A, B, C                               
REMARK 350   BIOMT1   1  1.000000  0.000000  0.000000        0.00000            
REMARK 350   BIOMT2   1  0.000000  1.000000  0.000000        0.00000            
REMARK 350   BIOMT3   1  0.000000  0.000000  1.000000        0.00000            
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: CLOSE CONTACTS                                             
REMARK 500                                                                      
REMARK 500 THE FOLLOWING ATOMS ARE IN CLOSE CONTACT.                            
REMARK 500                                                                      
REMARK 500  ATM1  RES C  SSEQI   ATM2  RES C  SSEQI           DISTANCE          
REMARK 500   H    LEU B    19     O    PHE B    31              1.13            
REMARK 500   O    LEU B    19     H    PHE B    31              1.25            
REMARK 500   O    PHE A    49     HB   ILE A    56              1.26            
REMARK 500   HA   HIS B    16     HA   LYS B    34              1.33            
REMARK 500   O    LYS A    10     H    ILE A    73              1.35            
REMARK 500   O    CYS B    47     H    GLY B    51              1.35            
REMARK 500   O    ALA B    22     H    VAL B    85              1.38            
REMARK 500   O    ARG A    50     H    ASP A    74              1.39            
REMARK 500   H    ARG A    48     O    PHE A    76              1.45            
REMARK 500   H    ARG A    50     O    ASP A    74              1.46            
REMARK 500   O    GLY A    13     H    ASP A    15              1.51            
REMARK 500   O    TYR B    46     H    GLN B    50              1.52            
REMARK 500   H    PHE B    61     O    GLN B    64              1.53            
REMARK 500  HD21  ASN B    18     O    PHE B    31              1.54            
REMARK 500   O    PHE B    61     H    GLN B    64              1.54            
REMARK 500   HA   VAL A    11     O    ILE A    73              1.55            
REMARK 500   H    VAL A    11     O    VAL A    19              1.57            
REMARK 500   O    PRO B    72     H    GLU B    76              1.58            
REMARK 500   O    ILE A     7     H    ILE A    23              1.58            
REMARK 500   O    ILE B    17     H    ILE B    33              1.58            
REMARK 500   O    GLN B    56     HE   ARG B    58              1.60            
REMARK 500   C    GLY A    82     NZ   LYS B    11              1.92            
REMARK 500   N    LEU B    19     O    PHE B    31              2.04            
REMARK 500   O    ARG A    50     N    ASP A    74              2.08            
REMARK 500   O    ASN B    67     N    THR B    69              2.12            
REMARK 500   O    LYS A    10     N    ILE A    73              2.13            
REMARK 500   O    PHE A    49     CB   ILE A    56              2.16            
REMARK 500   O    LEU B    19     N    PHE B    31              2.16            
REMARK 500   N    ARG A    50     O    ASP A    74              2.16            
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500 CHOLOGY: RAMACHANDRAN REVISITED. STRUCTURE 4, 1395 - 1400            
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  1 ASP A   5     -155.56    -89.56                                   
REMARK 500  1 GLN A  14      -36.49     60.95                                   
REMARK 500  1 HIS A  26       27.47    -69.18                                   
REMARK 500  1 GLN A  40     -106.63   -118.18                                   
REMARK 500  1 MET A  44      -15.00     80.70                                   
REMARK 500  1 ASP A  52       94.12    -37.88                                   
REMARK 500  1 GLN A  54      104.79   -170.64                                   
REMARK 500  1 MET A  67      -89.50    -85.23                                   
REMARK 500  1 GLU A  68     -179.74    117.92                                   
REMARK 500  1 GLU A  70       34.77   -157.26                                   
REMARK 500  1 GLN A  78      107.10     49.13                                   
REMARK 500  1 GLN A  79      -29.48    173.44                                   
REMARK 500  1 GLU B   4     -146.79    -99.24                                   
REMARK 500  1 LYS B   5      131.17     52.24                                   
REMARK 500  1 PRO B   6      103.27    -46.88                                   
REMARK 500  1 LYS B   7       93.41    -52.70                                   
REMARK 500  1 GLU B   8       61.93     60.90                                   
REMARK 500  1 LYS B  11      -76.29   -108.70                                   
REMARK 500  1 GLU B  13     -177.98     44.06                                   
REMARK 500  1 ASP B  15       40.60   -141.43                                   
REMARK 500  1 HIS B  16      139.42      9.58                                   
REMARK 500  1 SER B  27     -163.47   -161.44                                   
REMARK 500  1 LYS B  34     -116.94    -65.44                                   
REMARK 500  1 ARG B  35      -63.21   -158.41                                   
REMARK 500  1 MET B  54      -45.27     84.11                                   
REMARK 500  1 ASP B  62       94.27    -38.57                                   
REMARK 500  1 ILE B  66       25.69     48.79                                   
REMARK 500  1 ASN B  67       72.80    -20.40                                   
REMARK 500  1 GLU B  68      -67.90     32.71                                   
REMARK 500  1 THR B  69      -33.04    -39.68                                   
REMARK 500  1 GLU B  76       40.01     94.80                                   
REMARK 500  1 VAL C   3       93.47     41.57                                   
REMARK 500  1 ASP C   5     -176.33    -59.16                                   
REMARK 500  1 VAL C   8      -32.76   -178.58                                   
REMARK 500  1 ASP C   9      107.12     46.46                                   
REMARK 500  1 THR C  11     -166.85    -58.28                                   
REMARK 500  1 CYS C  12     -176.87    -54.03                                   
REMARK 500  1 VAL C  20      153.59    177.26                                   
REMARK 500  1 HIS C  24     -118.06    -94.96                                   
REMARK 500  1 ASN C  25      168.46     63.62                                   
REMARK 500  2 THR A   2      -86.44   -114.99                                   
REMARK 500  2 GLU A   3     -138.14   -150.36                                   
REMARK 500  2 ASN A   4       44.77    -83.67                                   
REMARK 500  2 HIS A  26       23.88    -75.71                                   
REMARK 500  2 LEU A  42      176.40    -57.83                                   
REMARK 500  2 MET A  44      -10.68     78.46                                   
REMARK 500  2 ASP A  52       94.83    -37.92                                   
REMARK 500  2 GLN A  54      104.13   -172.07                                   
REMARK 500  2 PRO A  55      144.99    -39.40                                   
REMARK 500  2 GLU A  70        2.55     93.83                                   
REMARK 500                                                                      
REMARK 500 THIS ENTRY HAS     478 RAMACHANDRAN OUTLIERS.                        
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 900                                                                      
REMARK 900 RELATED ENTRIES                                                      
REMARK 900 RELATED ID: 19961   RELATED DB: BMRB                                 
DBREF  2MP2 A    2    82  UNP    P55854   SUMO3_HUMAN     12     92             
DBREF  2MP2 B    2    90  UNP    P55854   SUMO3_HUMAN      2     90             
DBREF  2MP2 C    2    26  UNP    Q9QZS2   RNF4_MOUSE      45     69             
SEQADV 2MP2 GLY A    1  UNP  P55854          NaN EXPRESSION TAG                 
SEQADV 2MP2 GLY B    1  UNP  P55854          NaN EXPRESSION TAG                 
SEQRES   1 A   82  GLY THR GLU ASN ASP HIS ILE ASN LEU LYS VAL ALA GLY          
SEQRES   2 A   82  GLN ASP GLY SER VAL VAL GLN PHE LYS ILE LYS ARG HIS          
SEQRES   3 A   82  THR PRO LEU SER LYS LEU MET LYS ALA TYR CYS GLU ARG          
SEQRES   4 A   82  GLN GLY LEU SER MET ARG GLN ILE ARG PHE ARG PHE ASP          
SEQRES   5 A   82  GLY GLN PRO ILE ASN GLU THR ASP THR PRO ALA GLN LEU          
SEQRES   6 A   82  GLU MET GLU ASP GLU ASP THR ILE ASP VAL PHE GLN GLN          
SEQRES   7 A   82  GLN THR GLY GLY                                              
SEQRES   1 B   90  GLY SER GLU GLU LYS PRO LYS GLU GLY VAL LYS THR GLU          
SEQRES   2 B   90  ASN ASP HIS ILE ASN LEU LYS VAL ALA GLY GLN ASP GLY          
SEQRES   3 B   90  SER VAL VAL GLN PHE LYS ILE LYS ARG HIS THR PRO LEU          
SEQRES   4 B   90  SER LYS LEU MET LYS ALA TYR CYS GLU ARG GLN GLY LEU          
SEQRES   5 B   90  SER MET ARG GLN ILE ARG PHE ARG PHE ASP GLY GLN PRO          
SEQRES   6 B   90  ILE ASN GLU THR ASP THR PRO ALA GLN LEU GLU MET GLU          
SEQRES   7 B   90  ASP GLU ASP THR ILE ASP VAL PHE GLN GLN GLN THR              
SEQRES   1 C   25  THR VAL GLY ASP GLU ILE VAL ASP LEU THR CYS GLU SER          
SEQRES   2 C   25  LEU GLU PRO VAL VAL VAL ASP LEU THR HIS ASN ASP              
HELIX    1   1 LEU A   29  GLN A   40  1                                  12    
HELIX    2   2 PRO A   62  GLU A   66  5                                   5    
HELIX    3   3 LEU B   39  GLY B   51  1                                  13    
HELIX    4   4 THR B   71  GLU B   76  1                                   6    
SHEET    1   A 4 VAL A  18  LYS A  22  0                                        
SHEET    2   A 4 ASN A   8  ALA A  12 -1  N  VAL A  11   O  VAL A  19           
SHEET    3   A 4 ASP A  71  PHE A  76  1  O  ILE A  73   N  LYS A  10           
SHEET    4   A 4 ARG A  48  PHE A  51 -1  N  ARG A  48   O  PHE A  76           
SHEET    1   B 3 VAL B  29  ILE B  33  0                                        
SHEET    2   B 3 ILE B  17  GLY B  23 -1  N  LEU B  19   O  PHE B  31           
SHEET    3   B 3 ASP B  81  VAL B  85  1  O  VAL B  85   N  ALA B  22           
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
MODEL       10                                                                  
ATOM      2  CA  GLY A   1     286.397 -16.906 -14.756  1.00  0.00           C  
ATOM     11  CA  THR A   2     285.575 -14.281 -12.141  1.00  0.00           C  
ATOM     25  CA  GLU A   3     288.876 -14.766 -10.304  1.00  0.00           C  
ATOM     40  CA  ASN A   4     288.574 -11.087  -9.359  1.00  0.00           C  
ATOM     54  CA  ASP A   5     285.988 -11.967  -6.691  1.00  0.00           C  
ATOM     66  CA  HIS A   6     286.477 -12.134  -2.948  1.00  0.00           C  
ATOM     84  CA  ILE A   7     288.284 -14.434  -0.610  1.00  0.00           C  
ATOM    103  CA  ASN A   8     288.733 -14.328   3.129  1.00  0.00           C  
ATOM    117  CA  LEU A   9     292.215 -13.702   4.496  1.00  0.00           C  
ATOM    136  CA  LYS A  10     293.443 -14.588   7.911  1.00  0.00           C  
ATOM    158  CA  VAL A  11     295.760 -11.881   9.106  1.00  0.00           C  
ATOM    174  CA  ALA A  12     298.039 -13.385  11.744  1.00  0.00           C  
ATOM    184  CA  GLY A  13     299.810 -11.059  14.132  1.00  0.00           C  
ATOM    191  CA  GLN A  14     303.261 -11.103  15.664  1.00  0.00           C  
ATOM    208  CA  ASP A  15     301.573 -12.071  18.940  1.00  0.00           C  
ATOM    220  CA  GLY A  16     299.888 -15.116  17.408  1.00  0.00           C  
ATOM    227  CA  SER A  17     296.562 -13.372  16.848  1.00  0.00           C  
ATOM    238  CA  VAL A  18     295.138 -13.688  13.337  1.00  0.00           C  
ATOM    254  CA  VAL A  19     291.991 -12.042  11.915  1.00  0.00           C  
ATOM    270  CA  GLN A  20     289.575 -12.709   9.063  1.00  0.00           C  
ATOM    287  CA  PHE A  21     289.238 -10.332   6.144  1.00  0.00           C  
ATOM    307  CA  LYS A  22     287.877 -11.132   2.732  1.00  0.00           C  
ATOM    329  CA  ILE A  23     288.807  -8.842  -0.073  1.00  0.00           C  
ATOM    348  CA  LYS A  24     288.348  -8.959  -3.845  1.00  0.00           C  
ATOM    370  CA  ARG A  25     291.390 -10.768  -5.247  1.00  0.00           C  
ATOM    394  CA  HIS A  26     292.280  -7.944  -7.612  1.00  0.00           C  
ATOM    412  CA  THR A  27     292.126  -5.614  -4.630  1.00  0.00           C  
ATOM    426  CA  PRO A  28     295.129  -4.105  -2.884  1.00  0.00           C  
ATOM    440  CA  LEU A  29     295.631  -5.836   0.441  1.00  0.00           C  
ATOM    459  CA  SER A  30     296.209  -2.298   1.721  1.00  0.00           C  
ATOM    470  CA  LYS A  31     292.497  -2.505   2.542  1.00  0.00           C  
ATOM    492  CA  LEU A  32     292.544  -5.874   4.307  1.00  0.00           C  
ATOM    511  CA  MET A  33     295.442  -4.719   6.446  1.00  0.00           C  
ATOM    528  CA  LYS A  34     293.671  -1.407   6.932  1.00  0.00           C  
ATOM    550  CA  ALA A  35     291.001  -3.499   8.613  1.00  0.00           C  
ATOM    560  CA  TYR A  36     293.551  -5.369  10.759  1.00  0.00           C  
ATOM    581  CA  CYS A  37     295.049  -1.995  11.599  1.00  0.00           C  
ATOM    592  CA  GLU A  38     291.515  -0.840  12.238  1.00  0.00           C  
ATOM    607  CA  ARG A  39     290.889  -4.179  13.937  1.00  0.00           C  
ATOM    631  CA  GLN A  40     294.024  -4.446  16.033  1.00  0.00           C  
ATOM    648  CA  GLY A  41     294.993  -0.804  16.325  1.00  0.00           C  
ATOM    655  CA  LEU A  42     298.146  -0.663  14.208  1.00  0.00           C  
ATOM    674  CA  SER A  43     299.504   2.108  12.012  1.00  0.00           C  
ATOM    685  CA  MET A  44     299.803  -0.053   8.884  1.00  0.00           C  
ATOM    702  CA  ARG A  45     303.142   1.721   8.354  1.00  0.00           C  
ATOM    726  CA  GLN A  46     304.818   0.453  11.519  1.00  0.00           C  
ATOM    743  CA  ILE A  47     303.253  -2.878  10.608  1.00  0.00           C  
ATOM    762  CA  ARG A  48     304.100  -5.088   7.671  1.00  0.00           C  
ATOM    786  CA  PHE A  49     301.699  -7.785   6.648  1.00  0.00           C  
ATOM    806  CA  ARG A  50     303.299 -10.893   5.295  1.00  0.00           C  
ATOM    830  CA  PHE A  51     301.319 -13.436   3.408  1.00  0.00           C  
ATOM    850  CA  ASP A  52     301.688 -17.112   4.209  1.00  0.00           C  
ATOM    862  CA  GLY A  53     305.413 -16.977   3.700  1.00  0.00           C  
ATOM    869  CA  GLN A  54     305.477 -14.024   1.284  1.00  0.00           C  
ATOM    886  CA  PRO A  55     306.310 -10.375   2.103  1.00  0.00           C  
ATOM    900  CA  ILE A  56     303.366  -8.039   1.558  1.00  0.00           C  
ATOM    919  CA  ASN A  57     302.741  -4.664   0.031  1.00  0.00           C  
ATOM    933  CA  GLU A  58     299.865  -2.218   0.316  1.00  0.00           C  
ATOM    948  CA  THR A  59     299.077  -2.828  -3.348  1.00  0.00           C  
ATOM    962  CA  ASP A  60     299.767  -6.571  -3.239  1.00  0.00           C  
ATOM    974  CA  THR A  61     296.534  -8.495  -3.783  1.00  0.00           C  
ATOM    988  CA  PRO A  62     295.522 -12.091  -2.932  1.00  0.00           C  
ATOM   1002  CA  ALA A  63     295.589 -12.848  -6.683  1.00  0.00           C  
ATOM   1012  CA  GLN A  64     298.817 -11.008  -6.925  1.00  0.00           C  
ATOM   1029  CA  LEU A  65     299.820 -13.204  -4.035  1.00  0.00           C  
ATOM   1048  CA  GLU A  66     298.129 -16.379  -5.259  1.00  0.00           C  
ATOM   1063  CA  MET A  67     296.052 -16.382  -2.098  1.00  0.00           C  
ATOM   1080  CA  GLU A  68     292.801 -18.223  -1.351  1.00  0.00           C  
ATOM   1095  CA  ASP A  69     289.501 -17.994   0.372  1.00  0.00           C  
ATOM   1107  CA  GLU A  70     289.949 -18.099   4.116  1.00  0.00           C  
ATOM   1122  CA  ASP A  71     293.695 -18.026   3.455  1.00  0.00           C  
ATOM   1134  CA  THR A  72     296.034 -16.332   5.871  1.00  0.00           C  
ATOM   1148  CA  ILE A  73     297.737 -13.018   5.839  1.00  0.00           C  
ATOM   1167  CA  ASP A  74     300.706 -13.004   8.116  1.00  0.00           C  
ATOM   1179  CA  VAL A  75     301.563  -9.946  10.107  1.00  0.00           C  
ATOM   1195  CA  PHE A  76     304.885  -8.584  11.053  1.00  0.00           C  
ATOM   1215  CA  GLN A  77     305.491  -5.168  12.449  1.00  0.00           C  
ATOM   1232  CA  GLN A  78     308.188  -3.400  10.441  1.00  0.00           C  
ATOM   1249  CA  GLN A  79     310.068  -6.359   8.955  1.00  0.00           C  
ATOM   1266  CA  THR A  80     309.552  -7.068   5.255  1.00  0.00           C  
ATOM   1280  CA  GLY A  81     310.418  -4.525   2.572  1.00  0.00           C  
ATOM   1287  CA  GLY A  82     309.994  -7.255  -0.018  1.00  0.00           C  
TER    1293      GLY A  82                                                      
ATOM   1295  CA  GLY B   1     311.315   3.981  11.272  1.00  0.00           C  
ATOM   1304  CA  SER B   2     308.243   5.675   9.821  1.00  0.00           C  
ATOM   1315  CA  GLU B   3     308.345   4.786   6.127  1.00  0.00           C  
ATOM   1330  CA  GLU B   4     311.865   3.358   5.969  1.00  0.00           C  
ATOM   1345  CA  LYS B   5     313.624   1.034   3.548  1.00  0.00           C  
ATOM   1367  CA  PRO B   6     313.809  -2.771   3.259  1.00  0.00           C  
ATOM   1381  CA  LYS B   7     316.515  -4.088   5.582  1.00  0.00           C  
ATOM   1403  CA  GLU B   8     318.179  -5.650   2.568  1.00  0.00           C  
ATOM   1418  CA  GLY B   9     315.007  -6.318   0.563  1.00  0.00           C  
ATOM   1425  CA  VAL B  10     316.601  -5.991  -2.812  1.00  0.00           C  
ATOM   1441  CA  LYS B  11     314.649  -8.689  -4.610  1.00  0.00           C  
ATOM   1461  CA  THR B  12     315.312  -7.535  -8.148  1.00  0.00           C  
ATOM   1475  CA  GLU B  13     316.196  -3.821  -8.368  1.00  0.00           C  
ATOM   1490  CA  ASN B  14     313.324  -1.331  -7.788  1.00  0.00           C  
ATOM   1504  CA  ASP B  15     313.001  -2.858  -4.288  1.00  0.00           C  
ATOM   1516  CA  HIS B  16     314.594   0.288  -2.767  1.00  0.00           C  
ATOM   1534  CA  ILE B  17     317.743  -0.135  -0.674  1.00  0.00           C  
ATOM   1553  CA  ASN B  18     319.570   2.085   1.750  1.00  0.00           C  
ATOM   1567  CA  LEU B  19     322.603   3.842   0.313  1.00  0.00           C  
ATOM   1586  CA  LYS B  20     325.588   5.411   1.964  1.00  0.00           C  
ATOM   1608  CA  VAL B  21     327.607   8.367   0.744  1.00  0.00           C  
ATOM   1624  CA  ALA B  22     331.048   9.030   2.165  1.00  0.00           C  
ATOM   1634  CA  GLY B  23     332.190  12.639   2.064  1.00  0.00           C  
ATOM   1641  CA  GLN B  24     335.336  14.604   2.869  1.00  0.00           C  
ATOM   1658  CA  ASP B  25     333.716  15.126   6.276  1.00  0.00           C  
ATOM   1670  CA  GLY B  26     335.508  11.950   7.343  1.00  0.00           C  
ATOM   1677  CA  SER B  27     332.420   9.799   7.687  1.00  0.00           C  
ATOM   1688  CA  VAL B  28     329.385   8.542   5.824  1.00  0.00           C  
ATOM   1704  CA  VAL B  29     325.720   9.210   5.223  1.00  0.00           C  
ATOM   1720  CA  GLN B  30     322.942   6.767   4.647  1.00  0.00           C  
ATOM   1737  CA  PHE B  31     320.564   7.313   1.756  1.00  0.00           C  
ATOM   1757  CA  LYS B  32     317.662   5.147   0.665  1.00  0.00           C  
ATOM   1779  CA  ILE B  33     317.351   4.639  -3.031  1.00  0.00           C  
ATOM   1798  CA  LYS B  34     315.107   2.329  -5.062  1.00  0.00           C  
ATOM   1820  CA  ARG B  35     317.814   0.918  -7.377  1.00  0.00           C  
ATOM   1844  CA  HIS B  36     316.865   1.621 -11.001  1.00  0.00           C  
ATOM   1862  CA  THR B  37     316.804   5.337 -10.176  1.00  0.00           C  
ATOM   1876  CA  PRO B  38     319.457   7.772 -11.514  1.00  0.00           C  
ATOM   1890  CA  LEU B  39     321.681   8.634  -8.548  1.00  0.00           C  
ATOM   1909  CA  SER B  40     321.322  12.349  -9.505  1.00  0.00           C  
ATOM   1920  CA  LYS B  41     318.756  12.436  -6.689  1.00  0.00           C  
ATOM   1942  CA  LEU B  42     321.233  10.767  -4.379  1.00  0.00           C  
ATOM   1961  CA  MET B  43     323.852  13.263  -5.520  1.00  0.00           C  
ATOM   1978  CA  LYS B  44     321.774  16.283  -4.448  1.00  0.00           C  
ATOM   2000  CA  ALA B  45     320.874  14.910  -1.027  1.00  0.00           C  
ATOM   2010  CA  TYR B  46     324.477  14.923   0.077  1.00  0.00           C  
ATOM   2031  CA  CYS B  47     324.653  18.466  -1.292  1.00  0.00           C  
ATOM   2042  CA  GLU B  48     321.558  19.816   0.402  1.00  0.00           C  
ATOM   2057  CA  ARG B  49     323.149  17.932   3.296  1.00  0.00           C  
ATOM   2081  CA  GLN B  50     326.634  19.496   3.329  1.00  0.00           C  
ATOM   2098  CA  GLY B  51     325.912  22.458   1.077  1.00  0.00           C  
ATOM   2105  CA  LEU B  52     327.626  20.940  -1.948  1.00  0.00           C  
ATOM   2124  CA  SER B  53     326.926  22.159  -5.470  1.00  0.00           C  
ATOM   2135  CA  MET B  54     325.777  18.508  -5.940  1.00  0.00           C  
ATOM   2152  CA  ARG B  55     327.238  18.569  -9.401  1.00  0.00           C  
ATOM   2176  CA  GLN B  56     330.657  19.676  -8.191  1.00  0.00           C  
ATOM   2193  CA  ILE B  57     331.164  16.783  -5.769  1.00  0.00           C  
ATOM   2212  CA  ARG B  58     332.472  13.465  -6.946  1.00  0.00           C  
ATOM   2236  CA  PHE B  59     330.761  10.174  -6.243  1.00  0.00           C  
ATOM   2256  CA  ARG B  60     332.087   6.667  -6.491  1.00  0.00           C  
ATOM   2280  CA  PHE B  61     330.527   3.358  -5.601  1.00  0.00           C  
ATOM   2300  CA  ASP B  62     332.591   0.594  -3.944  1.00  0.00           C  
ATOM   2312  CA  GLY B  63     334.859  -0.080  -6.924  1.00  0.00           C  
ATOM   2319  CA  GLN B  64     332.946   1.720  -9.653  1.00  0.00           C  
ATOM   2336  CA  PRO B  65     332.263   5.412 -10.447  1.00  0.00           C  
ATOM   2350  CA  ILE B  66     328.589   6.360 -10.484  1.00  0.00           C  
ATOM   2369  CA  ASN B  67     328.264   9.034 -13.110  1.00  0.00           C  
ATOM   2383  CA  GLU B  68     325.127  10.341 -11.340  1.00  0.00           C  
ATOM   2398  CA  THR B  69     322.998   9.307 -14.294  1.00  0.00           C  
ATOM   2412  CA  ASP B  70     323.937   5.771 -13.254  1.00  0.00           C  
ATOM   2424  CA  THR B  71     321.332   4.225 -11.024  1.00  0.00           C  
ATOM   2438  CA  PRO B  72     322.494   1.964  -8.186  1.00  0.00           C  
ATOM   2452  CA  ALA B  73     320.825  -1.167  -9.599  1.00  0.00           C  
ATOM   2462  CA  GLN B  74     322.172  -0.432 -13.058  1.00  0.00           C  
ATOM   2479  CA  LEU B  75     325.552  -0.499 -11.381  1.00  0.00           C  
ATOM   2498  CA  GLU B  76     324.498  -3.285  -8.975  1.00  0.00           C  
ATOM   2513  CA  MET B  77     324.363  -1.607  -5.561  1.00  0.00           C  
ATOM   2530  CA  GLU B  78     323.071  -3.322  -2.409  1.00  0.00           C  
ATOM   2545  CA  ASP B  79     321.469  -2.067   0.782  1.00  0.00           C  
ATOM   2557  CA  GLU B  80     323.810  -0.140   3.127  1.00  0.00           C  
ATOM   2572  CA  ASP B  81     326.064   0.355   0.112  1.00  0.00           C  
ATOM   2584  CA  THR B  82     328.412   3.319   0.267  1.00  0.00           C  
ATOM   2598  CA  ILE B  83     329.355   5.931  -2.307  1.00  0.00           C  
ATOM   2617  CA  ASP B  84     332.503   7.966  -1.791  1.00  0.00           C  
ATOM   2629  CA  VAL B  85     332.310  11.735  -2.318  1.00  0.00           C  
ATOM   2645  CA  PHE B  86     335.577  13.494  -3.127  1.00  0.00           C  
ATOM   2665  CA  GLN B  87     335.548  17.161  -3.940  1.00  0.00           C  
ATOM   2682  CA  GLN B  88     338.222  19.743  -3.122  1.00  0.00           C  
ATOM   2699  CA  GLN B  89     336.795  22.943  -1.638  1.00  0.00           C  
ATOM   2716  CA  THR B  90     338.136  26.452  -2.238  1.00  0.00           C  
TER    2730      THR B  90                                                      
ATOM   2732  CA  THR C   2     291.109  -5.291  19.276  1.00  0.00           C  
ATOM   2748  CA  VAL C   3     287.514  -5.970  18.228  1.00  0.00           C  
ATOM   2764  CA  GLY C   4     287.465  -8.906  15.825  1.00  0.00           C  
ATOM   2771  CA  ASP C   5     286.555  -9.698  12.220  1.00  0.00           C  
ATOM   2783  CA  GLU C   6     286.098  -6.717   9.894  1.00  0.00           C  
ATOM   2798  CA  ILE C   7     286.124  -7.785   6.243  1.00  0.00           C  
ATOM   2817  CA  VAL C   8     285.466  -5.400   3.337  1.00  0.00           C  
ATOM   2833  CA  ASP C   9     284.049  -5.322  -0.212  1.00  0.00           C  
ATOM   2845  CA  LEU C  10     285.903  -3.662  -3.122  1.00  0.00           C  
ATOM   2864  CA  THR C  11     288.455  -4.220  -5.932  1.00  0.00           C  
ATOM   2878  CA  CYS C  12     290.912  -1.439  -6.773  1.00  0.00           C  
ATOM   2889  CA  GLU C  13     293.083  -1.172  -9.887  1.00  0.00           C  
ATOM   2904  CA  SER C  14     296.277   0.404 -11.244  1.00  0.00           C  
ATOM   2915  CA  LEU C  15     299.804  -0.279  -9.992  1.00  0.00           C  
ATOM   2934  CA  GLU C  16     302.024   1.605  -7.544  1.00  0.00           C  
ATOM   2949  CA  PRO C  17     304.573   4.243  -8.707  1.00  0.00           C  
ATOM   2963  CA  VAL C  18     308.253   4.311  -7.751  1.00  0.00           C  
ATOM   2979  CA  VAL C  19     310.402   7.066  -6.248  1.00  0.00           C  
ATOM   2995  CA  VAL C  20     313.683   7.169  -4.326  1.00  0.00           C  
ATOM   3011  CA  ASP C  21     314.640   9.573  -1.518  1.00  0.00           C  
ATOM   3023  CA  LEU C  22     317.106  10.271   1.309  1.00  0.00           C  
ATOM   3042  CA  THR C  23     315.704   7.743   3.799  1.00  0.00           C  
ATOM   3056  CA  HIS C  24     314.434   9.030   7.155  1.00  0.00           C  
ATOM   3074  CA  ASN C  25     317.595   8.799   9.259  1.00  0.00           C  
ATOM   3088  CA  ASP C  26     321.054  10.371   9.072  1.00  0.00           C  
TER    3100      ASP C  26                                                      
ENDMDL                                                                          
MASTER      185    0    0    4    7    0    0    6 1566    3    0   16          
END                                                                             
