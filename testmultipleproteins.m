proteins = ['2lme';'2m3x';'2JUV';'2RVB';'2MXR';'2MW2';'2MJO';'2n1f';...
                                        '2m6i';'2mxu';'2MP2';'2m3b'];
proteins = cellstr(proteins);

for i=1:length(proteins)
    name = char(strcat('samples/test/', proteins(i), '.pdb'));
    destination = char(strcat('samples/results/',proteins(i),'_opt1000_3rd_v2.pdb'));
    insulin_demo(name, destination);
end


proteins = ['2lme';'2m3x';'2MP2';'2RVB';'2m6i';'2mxu';'2m3b'];
proteins = cellstr(proteins);

for i=1:length(proteins)
    name = char(strcat('samples/test/', proteins(i), '.pdb'));
    destination = char(strcat('samples/results/',proteins(i),'_opt1000_300_4th_v2.pdb'));
    demo2(name, destination);
end