function [] = insulin_demo(file, name)

file0 = 'samples/insulin/new_insulin.pdb'; 
file1 = 'samples/calmodulin/calmodulin.pdb';
file2 = 'samples/short_insulin.pdb';
file3 = 'samples/2MP2/2MP2.pdb';
file4 = 'samples/2MXR.pdb';
file5 = 'samples/2LE9.pdb';
file6 = 'samples/KcsA.pdb';
file7 = 'samples/hemoglobin.pdb';
file8 = 'samples/final_KcsA.pdb';
file9 = 'samples/2LWW/2LWW.pdb';
file10 = 'samples/2RVB/2RVB.pdb';
file11 = 'samples/2myj/2myj.pdb';
file12 = 'samples/2m3x/2m3x.pdb';
file13 = 'samples/2m6i/2m6i.pdb';
file14 = 'samples/2k1e/2k1e.pdb';
file15 = 'samples/2n1f/2n1f.pdb';
file16 = 'samples/2mxu/2mxu.pdb';
file17 = 'samples/2mse/2mse.pdb';
file18 = 'samples/2mse/2mse_clean.pdb';
file19 = 'samples/2n1f/2n1f_altered.pdb';
file20 = 'samples/2mse/2mse_normal.pdb';
file21 = 'samples/2lme/2lme.pdb';
file22 = 'samples/tests/2m3b.pdb';
file23 = 'samples/2MJO/2MJO.pdb';
file24 = 'samples/2MW2/2MW2.pdb';
file25 = 'samples/2MXR/2MXR.pdb';


[initialPDBstruct, firstConf, lastConf] = getData(file); 

nConf = 8;
model = trmcreate(firstConf, lastConf, nConf);

angleIndices = trmdistantangleindices(model);
f = @(x) trmobjfunc(model, angleIndices, x);

initial_point = createInitialPoint(model, angleIndices);

iterNum = 1000;

options = optimoptions('fmincon');
options = optimoptions(options,'Display', 'iter');%off
options = optimoptions(options,'MaxIter', iterNum);
options = optimoptions(options,'MaxFunEvals', Inf);
options = optimoptions(options,'GradObj','off');
options = optimoptions(options,'UseParallel',true);

[lowerBound, upperBound] = getBounds(model, angleIndices);

if max(size(gcp)) == 0 
    parpool
end

x = fmincon(f,initial_point,[],[],[],[],lowerBound, upperBound,[],...
    options);

modelOptimized = modelOpt(model, angleIndices, x);

delete(gcp);

toolboxPath = fileparts(which('calmodulin_demo'));
newPDB = trm2pdb(modelOptimized, initialPDBstruct);
pdbwrite(fullfile(toolboxPath, name), newPDB);

end

